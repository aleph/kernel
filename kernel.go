package kernel

import (
	"sync"

	"gitlab.com/aleph-project/form"
)

type Operation func(input form.Form, scope Scope, kernel Kernel) (result form.Form, altered bool, err error)

type Kernel interface {
	State() Context
	NewScope() Kernel
	NewWithScope(scope Scope) Kernel
	Evaluate(form.Form) (form.Form, error)
	EvaluateWithScope(form.Form, Scope) (form.Form, error)

	BuildRecognizer(predicate form.Form) (Recognizer, error)
}

type Recognizer interface {
	Predicate() form.Form
	Accept(n uint32) bool
	Recognize(kernel Kernel, scope Scope, exprs ...form.Form) (ok bool)
}

type Context interface {
	Parent() Context
	Symbols() SymbolTable

	GetSymbol(form.SymbolForm) (SymbolData, bool)
	GetOrCreateSymbol(form.SymbolForm) SymbolData
	InstallSymbol(form.SymbolForm, SymbolData) bool
}

type SymbolTable interface {
	Writable() bool

	Get(form.SymbolForm) (SymbolData, bool)
	GetOrCreate(form.SymbolForm) SymbolData
	Create(form.SymbolForm) (SymbolData, bool)

	GetByID(id uint64) (SymbolData, bool)
	GetByName(name string) (SymbolData, bool)

	Operate(SymbolTableOperation) error
}

type SymbolTableOperation func(lock *sync.RWMutex, get SymbolTableGet, create SymbolTableCreate) error
type SymbolTableGet func(form.SymbolForm) (SymbolData, bool)
type SymbolTableCreate func(form.SymbolForm) SymbolData

type SymbolData interface {
	sync.Locker
	RLocker() sync.Locker

	ID() uint64
	Name() form.SymbolForm
	Attributes() Attributes

	OwnValues() []SymbolValue
	InnerValues() []SymbolValue
	OuterValues() []SymbolValue

	SetOwnValues(clear bool, values ...SymbolValue)
	SetInnerValues(clear bool, values ...SymbolValue)
	SetOuterValues(clear bool, values ...SymbolValue)
}

type Attributes interface {
	Protected() bool
	Locked() bool

	HoldFirst() bool
	HoldRest() bool
	HoldAll() bool
	Freeze() bool

	Supported(attr form.SymbolForm) bool
	Set(form.SymbolForm) bool
	Clear(form.SymbolForm) bool
	Reset()

	Symbolize() []form.SymbolForm
}

type SymbolValue interface {
	Predicate() form.Form
	Definition() form.Form
	Returns() form.Form

	NewScope() Scope
	Recognize(kernel Kernel, expr form.Form, extractor Scope) bool
	Evaluate(input form.Form, extracted Scope, kernel Kernel) (result form.Form, altered bool, err error)
}

type Scope interface {
	Set(name string, value form.Form) Scope
	Get(name string) (form.Form, bool)
	Enumerate(fn func(string, form.Form))
}
