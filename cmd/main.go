package main

import (
	"log"
	"os"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel/debug"
	"gitlab.com/aleph-project/kernel/impl"
	"gitlab.com/aleph-project/link"
)

func main() {
	log.SetFlags(0)
	debug.SetFlags(debug.LogFile | debug.LogLine)

	var port = "localhost:8754"
	if len(os.Args) > 1 {
		port = os.Args[1]
	}

	l, err := link.Listen(true, "tcp", port)
	if err != nil {
		log.Fatal(err)
	}

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatal(err)
		}

		go handle(conn)
	}
}

func handle(conn link.Connection) {
	kernel := impl.NewKernel()

	for {
		ok := conn.Accept(func(f form.Form) (form.Form, error) {
			r, err := kernel.Evaluate(f)
			if r == nil {
				return form.Null, err
			}
			return r, err
		})
		if !ok {
			break
		}
	}
}
