module aleph-kernel

require (
	gitlab.com/aleph-project/extensions/cas v0.0.0-20181210060556-d1762ea8a856
	gitlab.com/aleph-project/extensions/hello v0.0.0-20181207064125-172482efd622
	gitlab.com/aleph-project/form v0.2.10
	gitlab.com/aleph-project/kernel v0.0.0-20181210051030-3842d83302d6
	gitlab.com/aleph-project/link v0.0.0-20181207051724-e4342ac24b5c
)

replace (
	gitlab.com/aleph-project/extensions/cas => ../../extensions/cas
	gitlab.com/aleph-project/extensions/hello => ../../extensions/hello
	gitlab.com/aleph-project/kernel => ../
	gitlab.com/aleph-project/link => ../../link
)
