package stub

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
)

var s = struct {
	Kernel      *Kernel
	Context     *Context
	SymbolTable *SymbolTable
}{}

type Kernel struct{}

func (*Kernel) State() kernel.Context                         { return s.Context }
func (*Kernel) NewScope() kernel.Kernel                       { panic("not implemented") }
func (*Kernel) Evaluate(form.Form) (form.Form, error)         { panic("not implemented") }
func (*Kernel) NewWithScope(scope kernel.Scope) kernel.Kernel { panic("not implemented") }
func (*Kernel) EvaluateWithScope(form.Form, kernel.Scope) (form.Form, error) {
	panic("not implemented")
}
func (*Kernel) BuildRecognizer(predicate form.Form) (kernel.Recognizer, error) {
	panic("not implemented")
}

type Context struct{}

func (*Context) Parent() kernel.Context                                { return nil }
func (*Context) Symbols() kernel.SymbolTable                           { return s.SymbolTable }
func (*Context) GetSymbol(form.SymbolForm) (kernel.SymbolData, bool)   { return nil, false }
func (*Context) GetOrCreateSymbol(form.SymbolForm) kernel.SymbolData   { panic("not implemented") }
func (*Context) InstallSymbol(form.SymbolForm, kernel.SymbolData) bool { return false }

type SymbolTable struct{}

func (*SymbolTable) Writable() bool                                   { return false }
func (*SymbolTable) Get(form.SymbolForm) (kernel.SymbolData, bool)    { return nil, false }
func (*SymbolTable) GetOrCreate(form.SymbolForm) kernel.SymbolData    { panic("not implemented") }
func (*SymbolTable) Create(form.SymbolForm) (kernel.SymbolData, bool) { return nil, false }
func (*SymbolTable) GetByID(id uint64) (kernel.SymbolData, bool)      { return nil, false }
func (*SymbolTable) GetByName(name string) (kernel.SymbolData, bool)  { return nil, false }
func (*SymbolTable) Operate(kernel.SymbolTableOperation) error        { panic("not implemented") }
