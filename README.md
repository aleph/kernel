# Aleph: Kernel

The computational kernel.

Components:
  * Context - keeps track of values
  * Recognizer - responsible for recognizing patterns
  * Transformer - applies transformation patterns
  * Operations - low-level operation implementations that cannot or should not be achieved via the transformer; must be pluggable
  * Server - kernel server process, which can be linked to by a client

## Plugins

**Note: Plugin capability is hidden behind the `plugins` build tag**, as compiling
Aleph with plugin support makes debugging problematic. See
[github.com/derekparker/delve#1439](https://github.com/derekparker/delve/issues/865)
and [github.com/golang/go#23733](https://github.com/golang/go/issues/23733).

A plugin can be loaded with `LoadPlugin["/path/to/plugin.so"]` and must have the
following exported symbols:

```go
import "gitlab.com/aleph-project/kernel"

func Install(kernel.Kernel) error
```