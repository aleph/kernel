package impl

import (
	"errors"
	"math"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel/symbols"
)

var (
	ErrOperationInputViolatesExpectations = errors.New("the inputs to the operation violated expectations")
)

var (
	realPosZeroValue = math.Copysign(0, +1)
	realNegZeroValue = math.Copysign(0, -1)
	realPosInfValue  = math.Inf(+1)
	realNegInfValue  = math.Inf(-1)
	realPosNaNValue  = +math.NaN()
	realNegNaNValue  = -math.NaN()
)

var (
	blankInteger  = symbols.NewBlank(symbols.Integer)
	blankRational = symbols.NewBlank(symbols.Rational)
	blankReal     = symbols.NewBlank(symbols.Real)
	matchNumeric  = symbols.NewForm(symbols.NumericHead)
	matchExact    = symbols.NewAlternatives(blankInteger, blankRational)
	emptyBlank    = symbols.NewEmptyBlank()
)

func getHead(x form.Form) (form.Form, bool, error) {
	switch y := x.(type) {
	case form.FormHead:
		return y.Form(), true, nil
	default:
		return form.Symbol(x.Head().Name()), true, nil
	}
}

func getHeadSymbol(el form.Form) (form.SymbolForm, bool) {
	sym, ok := el.(form.SymbolForm)
	if ok {
		return sym, true
	}

	f, ok := el.Head().(form.FormHead)
	if !ok {
		return form.SymbolForm(el.Head().Name()), true
	}

	sym, ok = f.Form().(form.SymbolForm)
	if ok {
		return sym, true
	}

	return "", false
}

func getSymbolValue(f form.Form) (string, bool) {
	if f == nil {
		return "", false
	}

	if s, ok := f.(form.SymbolForm); ok {
		return s.Raw(), true
	}

	p, ok := f.(form.PrimitiveForm)
	if !ok {
		return "", false
	}

	v, ok := p.Value().(string)
	if !ok {
		return "", false
	}
	return v, true
}

func getNumber(f form.Form, index uint32) (form.NumericForm, bool) {
	g, ok := f.GetElement(index)
	if !ok {
		return nil, false
	}
	h, ok := g.(form.NumericForm)
	if !ok {
		return nil, false
	}
	return h, true
}

func getNumbers(f form.Form) (values []form.NumericForm, large bool, ok bool) {
	var n = f.Depth()
	var v form.NumericForm

	values = make([]form.NumericForm, n)
	for i := uint32(0); i < n; i++ {
		v, ok = getNumber(f, i)
		if !ok {
			return
		}
		values[i] = v
		if v.IsLarge() {
			large = true
		}
	}

	return
}

func getString(f form.Form, index uint32) (form.StringForm, bool) {
	g, ok := f.GetElement(index)
	if !ok {
		return "", false
	}
	h, ok := g.(form.StringForm)
	if !ok {
		return "", false
	}
	return h, true
}

func getStrings(f form.Form) (values []form.StringForm, ok bool) {
	var n = f.Depth()
	var v form.StringForm

	values = make([]form.StringForm, n)
	for i := uint32(0); i < n; i++ {
		v, ok = getString(f, i)
		if !ok {
			return
		}
		values[i] = v
	}

	return
}

func getSymbol(f form.Form, index uint32) (form.SymbolForm, bool) {
	g, ok := f.GetElement(index)
	if !ok {
		return "", false
	}
	h, ok := g.(form.SymbolForm)
	if !ok {
		return "", false
	}
	return h, true
}

func getSymbols(f form.Form) (values []form.SymbolForm, ok bool) {
	var n = f.Depth()
	var v form.SymbolForm

	values = make([]form.SymbolForm, n)
	for i := uint32(0); i < n; i++ {
		v, ok = getSymbol(f, i)
		if !ok {
			return
		}
		values[i] = v
	}

	return
}

func getUUID(f form.Form, index uint32) (form.UUIDForm, bool) {
	g, ok := f.GetElement(index)
	if !ok {
		return form.UUIDForm{}, false
	}
	h, ok := g.(form.UUIDForm)
	if !ok {
		return form.UUIDForm{}, false
	}
	return h, true
}

func getUUIDs(f form.Form) (values []form.UUIDForm, ok bool) {
	var n = f.Depth()
	var v form.UUIDForm

	values = make([]form.UUIDForm, n)
	for i := uint32(0); i < n; i++ {
		v, ok = getUUID(f, i)
		if !ok {
			return
		}
		values[i] = v
	}

	return
}

func getValues(f form.Form, N uint32) (values []form.Form, ok bool) {
	var n = f.Depth()
	var v form.Form

	if N > 0 && n != N {
		return
	}

	values = make([]form.Form, n)
	for i := uint32(0); i < n; i++ {
		v, ok = f.GetElement(i)
		if !ok {
			return
		}
		values[i] = v
	}

	return
}
