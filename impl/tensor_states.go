package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

var LengthState = symbolInit{
	name: symbols.LengthForm,

	attributes: attrProtected,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewForm(
				symbols.LengthHead,
				symbols.NewPattern(symbols.NewEmptyBlank()),
			),
			operation: func(input form.Form, _ kernel.Scope, _ kernel.Kernel) (form.Form, bool, error) {
				x, ok := input.GetElement(0)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}

				return form.SmallIntegerForm(x.Depth()), true, nil
			},
		}.create(),
	},
}.create()

var DimensionsState = symbolInit{
	name: symbols.DimensionsForm,

	attributes: attrProtected,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewForm(
				symbols.DimensionsHead,
				symbols.NewPattern(symbols.NewBlank("Tensor")),
			),
			operation: func(input form.Form, _ kernel.Scope, _ kernel.Kernel) (form.Form, bool, error) {
				x, ok := input.GetElement(0)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}

				y, ok := x.(form.TensorForm)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}

				if y.Dimensionality() == 1 {
					return form.IntegerListForm{int64(y.Depth())}, true, nil
				}

				dims := y.GetDimensions()
				f := make(form.IntegerListForm, len(dims))
				for i, dim := range dims {
					f[i] = int64(dim)
				}

				return f, true, nil
			},
		}.create(),
	},
}.create()

var PartState = symbolInit{
	name: symbols.PartForm,

	attributes: attrProtected,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewForm(
				symbols.PartHead,
				symbols.NewPattern(symbols.NewEmptyBlank()),
				symbols.NewPattern(symbols.NewBlank("Integer")),
			),
			operation: func(input form.Form, _ kernel.Scope, _ kernel.Kernel) (form.Form, bool, error) {
				x, ok := input.GetElement(0)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}

				y, ok := input.GetElement(1)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}

				z, ok := y.(form.NumericForm)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}

				idx := z.SmallIntegerValue()
				if idx == 0 {
					return getHead(x)
				}
				if idx < 0 {
					// TODO return input, false, send error
					return symbols.NewStringError("Part index must be zero or positive"), true, nil
				}
				if uint32(idx) > x.Depth() {
					// TODO return input, false, send error
					return symbols.NewStringError("Part index is greater than form length"), true, nil
				}

				item, _ := x.GetElement(uint32(idx - 1))
				if item == nil {
					// TODO send error
					return input, false, nil
				}
				return item, true, nil
			},
		}.create(),
	},
}.create()
