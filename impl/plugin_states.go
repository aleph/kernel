// +build plugins

package impl

import (
	"path/filepath"
	"plugin"
	"sync"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

func initPluginStates() {
	PluginState.SetInnerValues(false, valueInit{
		makeScope: state.NullScope,
		predicate: symbols.NewPredicateWithTypes(symbols.PluginHead, false, symbols.UUID),
		operation: pluginLookupOperation,
	}.create())

	PluginsState.SetInnerValues(false, valueInit{
		makeScope: state.NullScope,
		predicate: symbols.NewPredicateWith(symbols.PluginsHead, false),
		operation: pluginsListOperation,
	}.create())

	LoadPluginState.SetInnerValues(false, valueInit{
		makeScope: state.NullScope,
		predicate: symbols.NewPredicateWithTypes(symbols.LoadPluginHead, false, symbols.String),
		operation: loadPluginOperation,
	}.create())
}

type pluginEntry struct {
	uuid form.UUIDForm
	path string
}

func (p *pluginEntry) Form() form.Form {
	return symbols.NewForm(symbols.PluginHead, p.uuid, form.StringForm(p.path))
}

var plugins = struct {
	sync.RWMutex
	data map[form.UUIDForm]*pluginEntry
}{
	data: map[form.UUIDForm]*pluginEntry{},
}

func pluginLookupOperation(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
	id, ok := getUUID(input, 0)
	if !ok {
		return input, false, ErrOperationInputViolatesExpectations
	}

	plugins.RLock()
	plugin, ok := plugins.data[id]
	plugins.RUnlock()

	if !ok {
		// did not find plugin
		return input, false, nil
	}

	return plugin.Form(), true, nil
}

func pluginsListOperation(form.Form, kernel.Scope, kernel.Kernel) (form.Form, bool, error) {
	plugins.RLock()
	list := make([]form.Form, 0, len(plugins.data))
	for _, p := range plugins.data {
		list = append(list, p.Form())
	}
	plugins.RUnlock()

	return form.ListForm(list), true, nil
}

func loadPluginOperation(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
	x, ok := getString(input, 0)
	if !ok {
		return input, false, ErrOperationInputViolatesExpectations
	}

	path := x.Raw()
	if !filepath.IsAbs(path) {
		// TODO send error and return null
		return symbols.NewError(form.String("Path must be absolute"), x), true, nil
	}

	p, err := plugin.Open(path)
	if err != nil {
		// TODO send error and return null
		return symbols.NewError(form.String("Failed to load plugin"), form.String(err.Error()), x), true, nil
	}

	sym, err := p.Lookup("Install")
	if err != nil {
		// TODO send error and return null
		return symbols.NewError(form.String("Failed to lookup plugin's installer"), form.String(err.Error()), x), true, nil
	}

	inst, ok := sym.(func(kernel.Kernel) error)
	if !ok {
		// TODO send error and return null
		return symbols.NewError(form.String("Symbol `Install` in plugin does not match expected signature: func Install(kernel.Kernel) error"), x), true, nil
	}

	err = inst(NewGlobalKernel())
	if err != nil {
		// TODO send error and return null
		return symbols.NewError(form.String("Failed to install plugin"), form.String(err.Error()), x), true, nil
	}

	id, err := NewRandomUUID()
	if err != nil {
		// really should not happen
		return input, false, err
	}

	plugin := &pluginEntry{id, path}
	plugins.Lock()
	plugins.data[id] = plugin
	plugins.Unlock()

	value := plugin.Form()
	LoadPluginState.SetInnerValues(false, valueInit{
		predicate:  input,
		definition: symbols.NewDefinition(input, value),
		operation: func(_ form.Form, extracted kernel.Scope, kern kernel.Kernel) (form.Form, bool, error) {
			return value, true, err
		},
	}.create())

	return value, true, nil
}
