package impl

import (
	"math/big"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

func initMathSymbols(data map[form.SymbolForm]*state.SymbolData) {
	data[symbols.RationalForm] = RationalState

	initMathSymbolIdentity(AddState, symbols.AddHead, form.Integer(0), true)
	initMathSymbolIdentity(AddState, symbols.AddHead, form.Integer(0), false)
	initMathSymbolIdentity(SubtractState, symbols.SubtractHead, form.Integer(0), false)
	initMathSymbolIdentity(MultiplyState, symbols.MultiplyHead, form.Integer(1), true)
	initMathSymbolIdentity(MultiplyState, symbols.MultiplyHead, form.Integer(1), false)
	initMathSymbolIdentity(DivideState, symbols.DivideHead, form.Integer(1), false)

	initMathSymbol(AddState, symbols.AddHead, addIntegerOperation, addRationalOperation, addRealOperation, true, blankInteger)
	initMathSymbol(SubtractState, symbols.SubtractHead, subtractIntegerOperation, subtractRationalOperation, subtractRealOperation, true, blankInteger)
	initMathSymbol(MultiplyState, symbols.MultiplyHead, multiplyIntegerOperation, multiplyRationalOperation, multiplyRealOperation, false, matchExact)
	initMathSymbol(DivideState, symbols.DivideHead, divideIntegerOperation, divideRationalOperation, divideRealOperation, false, matchExact)
}

func initMathSymbolIdentity(sym kernel.SymbolData, head form.Head, identity form.Form, left bool) {
	var value = valueInit{makeScope: state.NullScope}

	var index uint32
	if left {
		value.predicate = symbols.NewPredicateWith(head, false, identity, emptyBlank)
		index = 1
	} else {
		value.predicate = symbols.NewPredicateWith(head, false, emptyBlank, identity)
	}

	value.operation = func(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
		value, ok := input.GetElement(index)
		if !ok {
			return input, false, ErrOperationInputViolatesExpectations
		}
		return value, true, nil
	}

	sym.SetInnerValues(false, value.create())
}

func initMathSymbol(sym kernel.SymbolData, head form.Head, integerOperation, rationalOperation, realOperation kernel.Operation, unary bool, binaryIntegerReturns form.Form) {
	var add = func(op kernel.Operation, value *valueInit) {
		if op == nil {
			return
		}
		value.operation = op
		sym.SetInnerValues(false, value.create())
	}

	if unary {
		add(integerOperation, &valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWithTypes(head, false, symbols.Integer),
			returns:   blankInteger,
		})
		add(rationalOperation, &valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWithTypes(head, false, symbols.Rational),
			returns:   blankRational,
		})
		add(realOperation, &valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWithTypes(head, false, symbols.Real),
			returns:   blankReal,
		})
	}

	add(integerOperation, &valueInit{
		makeScope: state.NullScope,
		predicate: symbols.NewPredicateWithTypes(head, false, symbols.Integer, symbols.Integer),
		returns:   binaryIntegerReturns,
	})
	add(rationalOperation, &valueInit{
		makeScope: state.NullScope,
		predicate: symbols.NewPredicateWith(head, false, blankRational, matchExact),
		returns:   matchExact,
	})
	add(rationalOperation, &valueInit{
		makeScope: state.NullScope,
		predicate: symbols.NewPredicateWith(head, false, matchExact, blankRational),
		returns:   matchExact,
	})
	add(realOperation, &valueInit{
		makeScope: state.NullScope,
		predicate: symbols.NewPredicateWith(head, false, blankReal, matchNumeric),
		returns:   blankReal,
	})
	add(realOperation, &valueInit{
		makeScope: state.NullScope,
		predicate: symbols.NewPredicateWith(head, false, matchNumeric, blankReal),
		returns:   blankReal,
	})
}

var RationalState = symbolInit{
	name: symbols.RationalForm,

	attributes: attrLocked,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWithTypes(
				// the normal rational head can only be used for primitive
				// values; here we are using it so that `Rational(X, Y)` is
				// converted to the rational primitive `X/Y`
				form.NewUnknownSymbolicHead(symbols.Rational),
				false, symbols.Integer, symbols.Integer,
			),
			returns: symbols.NewBlank(symbols.Rational),
			operation: numericOperation(2, func(input form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
				if large {
					a := values[0].LargeIntegerValue()
					b := values[1].LargeIntegerValue()
					return form.LargeRational(new(big.Rat).SetFrac(a, b)), true, nil
				}

				a := values[0].SmallIntegerValue()
				b := values[1].SmallIntegerValue()
				return form.Rational(a, b), true, nil
			}),
		}.create(),
		// normalize rationals
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewBlank(symbols.Rational),
			returns:   symbols.NewBlank(symbols.Rational),
			operation: func(input form.Form, _ kernel.Scope, kern kernel.Kernel) (form.Form, bool, error) {
				value, ok := input.(form.RationalForm)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}

				// golang big.Rat automatically normalizes
				if value.IsLarge() {
					return input, false, nil
				}

				rat := value.SmallRationalValue()
				// X / 0 -> no change
				if rat.Denominator == 0 {
					return input, false, nil
				}
				return form.SmallRationalForm(rat.Reduce()), true, nil
			},
		}.create(),
	},
}.create()

var AddState = symbolInit{
	name: symbols.AddForm,

	attributes:  attrProtected,
	innerValues: []kernel.SymbolValue{},
}.create()

var SubtractState = symbolInit{
	name: symbols.SubtractForm,

	attributes: attrProtected,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWith(symbols.SubtractHead, false, form.Integer(0), emptyBlank),
			operation: func(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
				value, ok := input.GetElement(1)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}
				return symbols.NewForm(input.Head(), value), true, nil
			},
		}.create(),
	},
}.create()

var MultiplyState = symbolInit{
	name: symbols.MultiplyForm,

	attributes:  attrProtected,
	innerValues: []kernel.SymbolValue{},
}.create()

var DivideState = symbolInit{
	name: symbols.DivideForm,

	attributes:  attrProtected,
	innerValues: []kernel.SymbolValue{},
}.create()

var GCDState = symbolInit{
	name: symbols.GCDForm,

	attributes: attrProtected,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWithTypes(symbols.GCDHead, false, symbols.Integer, symbols.Integer),
			returns:   blankInteger,
			operation: gcdIntegerOperation,
		}.create(),
	},
}.create()
