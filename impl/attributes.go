package impl

import "gitlab.com/aleph-project/kernel/state"
import "gitlab.com/aleph-project/kernel/symbols"

var attrProtected = new(state.Attributes).CloneAndSet(symbols.ProtectedForm)
var attrLocked = attrProtected.CloneAndSet(symbols.LockedForm)
var attrHoldFirst = attrProtected.CloneAndSet(symbols.HoldFirstForm)
var attrHoldAll = attrProtected.CloneAndSet(symbols.HoldAllForm)
