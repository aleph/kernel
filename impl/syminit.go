package impl

import (
	"fmt"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
)

type symbolInit struct {
	name       form.SymbolForm
	attributes kernel.Attributes

	ownValues   []kernel.SymbolValue
	innerValues []kernel.SymbolValue
	outerValues []kernel.SymbolValue
}

func (i symbolInit) create() *state.SymbolData {
	s := state.NewSymbolData(i.name)

	for _, attr := range i.attributes.Symbolize() {
		if !s.Attributes().Set(attr) {
			panic(fmt.Errorf("failed to set attribute %v on %v", attr, i.name))
		}
	}

	s.SetOwnValues(false, i.ownValues...)
	s.SetInnerValues(false, i.innerValues...)
	s.SetOuterValues(false, i.outerValues...)

	return s
}

type valueInit struct {
	predicate  form.Form
	definition form.Form
	returns    form.Form
	operation  kernel.Operation
	makeScope  func() kernel.Scope
}

func (i valueInit) create() *state.SymbolValue {
	return state.NewSymbolValue(i.predicate, i.definition, i.returns, i.operation, i.makeScope)
}
