package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

func initUUIDSymbols(data map[form.SymbolForm]*state.SymbolData) {
	data[symbols.UUIDForm] = UUIDState
}

var UUIDState = symbolInit{
	name: symbols.RationalForm,

	attributes: attrLocked,
	innerValues: []kernel.SymbolValue{
		// UUID[]
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWith(form.NewUnknownSymbolicHead(symbols.UUID), false),
			returns:   symbols.NewBlank(symbols.UUID),
			operation: uuidRandomOperation,
		}.create(),

		// UUID["random"]
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWith(form.NewUnknownSymbolicHead(symbols.UUID), false, form.String("random")),
			returns:   symbols.NewBlank(symbols.UUID),
			operation: uuidRandomOperation,
		}.create(),

		// UUID["nil"]
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWith(form.NewUnknownSymbolicHead(symbols.UUID), false, form.String("nil")),
			returns:   symbols.NewBlank(symbols.UUID),
			operation: uuidNilOperation,
		}.create(),

		// UUID[()]
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWith(form.NewUnknownSymbolicHead(symbols.UUID), false, form.Null),
			returns:   symbols.NewBlank(symbols.UUID),
			operation: uuidNilOperation,
		}.create(),

		// UUID["xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"]
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWithTypes(form.NewUnknownSymbolicHead(symbols.UUID), false, symbols.String),
			returns:   symbols.NewBlank(symbols.UUID),
			operation: uuidParseOperation1,
		}.create(),

		// UUID["parse", "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"]
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWith(form.NewUnknownSymbolicHead(symbols.UUID), false, form.String("parse"), symbols.NewBlank(symbols.String)),
			returns:   symbols.NewBlank(symbols.UUID),
			operation: uuidParseOperation2,
		}.create(),
	},
}.create()
