package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

func newSymbolValue_Set(head form.Head, op kernel.Operation) kernel.SymbolValue {
	return valueInit{
		makeScope: state.NullScope,
		predicate: symbols.NewForm(
			head,
			symbols.NewPattern(symbols.NewEmptyBlank()),
			symbols.NewPattern(symbols.NewEmptyBlank()),
		),
		operation: op,
	}.create()
}

var SetState = symbolInit{
	name: symbols.SetForm,

	attributes: attrHoldFirst,
	innerValues: []kernel.SymbolValue{
		newSymbolValue_Set(symbols.SetHead, set),
	},
}.create()

var SetDelayedState = symbolInit{
	name: symbols.SetDelayedForm,

	attributes: attrHoldAll,
	innerValues: []kernel.SymbolValue{
		newSymbolValue_Set(symbols.SetDelayedHead, set),
	},
}.create()

var OutSetState = symbolInit{
	name: symbols.OutSetForm,

	attributes: attrHoldFirst,
	innerValues: []kernel.SymbolValue{
		newSymbolValue_Set(symbols.OutSetHead, outSet),
	},
}.create()

var OutSetDelayedState = symbolInit{
	name: symbols.OutSetDelayedForm,

	attributes: attrHoldAll,
	innerValues: []kernel.SymbolValue{
		newSymbolValue_Set(symbols.OutSetDelayedHead, outSet),
	},
}.create()
