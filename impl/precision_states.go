package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

var NState = symbolInit{
	name: symbols.NForm,

	attributes: attrProtected,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWith(symbols.NHead, false, emptyBlank),
			returns:   blankInteger,
			operation: func(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
				value, ok := input.GetElement(0)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}

				var walk func(value form.Form) (form.Form, error)
				walk = func(value form.Form) (form.Form, error) {
					if num, ok := value.(form.NumericForm); ok {
						if num.IsLarge() {
							return (*form.LargeRealForm)(num.LargeRealValue()), nil
						}
						return form.SmallRealForm(num.SmallRealValue()), nil
					}

					if prim, ok := value.(form.PrimitiveForm); ok {
						return prim, nil
					}

					head := value.Head()
					if f, ok := head.(form.FormHead); ok {
						v, err := walk(f.Form())
						if err != nil {
							return nil, err
						}
						head = form.NewFormHead(v)
					}

					var raw = value.GetElements()
					var els = make([]form.Form, len(raw))
					for i, el := range raw {
						v, err := walk(el)
						if err != nil {
							return nil, err
						}
						els[i] = v
					}

					return form.New(head, els...)
				}

				v, err := walk(value)
				return v, true, err
			},
		}.create(),
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWith(symbols.NHead, false, emptyBlank, symbols.MachinePrecisionForm),
			returns:   blankInteger,
			operation: func(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
				return symbols.NewStringError("TODO"), true, nil
			},
		}.create(),
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWith(symbols.NHead, false, emptyBlank, blankInteger),
			returns:   blankInteger,
			operation: func(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
				// TODO send error and return null
				return symbols.NewStringError("Specifying precision is not implemented"), true, nil
			},
		}.create(),
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewPredicateWith(symbols.NHead, false, emptyBlank, emptyBlank),
			returns:   blankInteger,
			operation: func(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
				value, ok := input.GetElement(1)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}

				// TODO send error and return null
				return symbols.NewError(form.String("Invalid precision"), value), true, nil
			},
		}.create(),
	},
}.create()
