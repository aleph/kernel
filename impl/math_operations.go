package impl

import (
	"math/big"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/form/value"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/debug"
)

func numericOperation(N uint32, op func(form.Form, []form.NumericForm, bool) (form.Form, bool, error)) kernel.Operation {
	return func(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
		if N > 0 && N != input.Depth() {
			return input, false, ErrOperationInputViolatesExpectations
		}

		values, large, ok := getNumbers(input)
		if !ok || N > 0 && N != uint32(len(values)) {
			return input, false, ErrOperationInputViolatesExpectations
		}

		return op(input, values, large)
	}
}

var addIntegerOperation = numericOperation(0, func(_ form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("adding %v", values)
	if large {
		var r = new(big.Int)
		for _, v := range values {
			r = r.Add(r, v.LargeIntegerValue())
		}
		return form.LargeInteger(r), true, nil
	}

	// TODO check for overflow
	var r int64
	for _, v := range values {
		r += v.SmallIntegerValue()
	}
	return form.Integer(r), true, nil
})

var addRationalOperation = numericOperation(0, func(_ form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("adding %v", values)
	if large {
		var r = new(big.Rat)
		for _, v := range values {
			r = r.Add(r, v.LargeRationalValue())
		}
		return form.LargeRational(r), true, nil
	}

	var r = value.Rational{Numerator: 0, Denominator: 1}
	for _, v := range values {
		r = r.Add(v.SmallRationalValue())
	}
	return form.Rational(r.Numerator, r.Denominator), true, nil
})

var addRealOperation = numericOperation(0, func(_ form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("adding %v", values)
	if large {
		var r = new(big.Float)
		for _, v := range values {
			r = r.Add(r, v.LargeRealValue())
		}
		return form.LargeReal(r), true, nil
	}

	// TODO check for overflow
	var r float64
	for _, v := range values {
		r += v.SmallRealValue()
	}
	return form.Real(r), true, nil
})

var subtractIntegerOperation = numericOperation(0, func(_ form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("subtracting %v", values)
	if len(values) == 1 {
		if large {
			r := values[0].LargeIntegerValue()
			r = r.Neg(r)
			return (*form.LargeIntegerForm)(r), true, nil
		}
		r := -values[0].SmallIntegerValue()
		return form.SmallIntegerForm(r), true, nil
	}

	if large {
		var r = values[0].LargeIntegerValue()
		for _, v := range values[1:] {
			r = r.Sub(r, v.LargeIntegerValue())
		}
		return form.LargeInteger(r), true, nil
	}

	// TODO check for overflow
	var r = values[0].SmallIntegerValue()
	for _, v := range values[1:] {
		r -= v.SmallIntegerValue()
	}
	return form.Integer(r), true, nil
})

var subtractRationalOperation = numericOperation(0, func(_ form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("subtracting %v", values)
	if len(values) == 1 {
		if large {
			r := values[0].LargeRationalValue()
			r = r.Neg(r)
			return (*form.LargeRationalForm)(r), true, nil
		}
		r := values[0].SmallRationalValue()
		r.Numerator = -r.Numerator
		return form.SmallRationalForm(r), true, nil
	}

	if large {
		var r = values[0].LargeRationalValue()
		for _, v := range values[1:] {
			r = r.Sub(r, v.LargeRationalValue())
		}
		return form.LargeRational(r), true, nil
	}

	// TODO check for overflow
	var r = values[0].SmallRationalValue()
	for _, v := range values[1:] {
		r = r.Subtract(v.SmallRationalValue())
	}
	return form.Rational(r.Numerator, r.Denominator), true, nil
})

var subtractRealOperation = numericOperation(0, func(_ form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("subtracting %v", values)
	if len(values) == 1 {
		if large {
			r := values[0].LargeRealValue()
			r = r.Neg(r)
			return (*form.LargeRealForm)(r), true, nil
		}
		r := -values[0].SmallRealValue()
		return form.SmallRealForm(r), true, nil
	}

	if large {
		var r = values[0].LargeRealValue()
		for _, v := range values[1:] {
			r = r.Sub(r, v.LargeRealValue())
		}
		return form.LargeReal(r), true, nil
	}

	// TODO check for overflow
	var r = values[0].SmallRealValue()
	for _, v := range values[1:] {
		r -= v.SmallRealValue()
	}
	return form.Real(r), true, nil
})

var multiplyIntegerOperation = numericOperation(0, func(_ form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("multiplying %v", values)
	if large {
		var r = big.NewInt(1)
		for _, v := range values {
			r = r.Mul(r, v.LargeIntegerValue())
		}
		return form.LargeInteger(r), true, nil
	}

	// TODO check for overflow
	var r int64 = 1
	for _, v := range values {
		r *= v.SmallIntegerValue()
	}
	return form.Integer(r), true, nil
})

var multiplyRationalOperation = numericOperation(0, func(_ form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("multiplying %v", values)
	if large {
		var r = big.NewRat(1, 1)
		for _, v := range values {
			r = r.Mul(r, v.LargeRationalValue())
		}
		return form.LargeRational(r), true, nil
	}

	var r = value.Rational{Numerator: 1, Denominator: 1}
	for _, v := range values {
		r = r.Multiply(v.SmallRationalValue())
	}
	return form.Rational(r.Numerator, r.Denominator), true, nil
})

var multiplyRealOperation = numericOperation(0, func(_ form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("multiplying %v", values)
	if large {
		var r = big.NewFloat(1)
		for _, v := range values {
			r = r.Mul(r, v.LargeRealValue())
		}
		return form.LargeReal(r), true, nil
	}

	// TODO check for overflow
	var r float64 = 1
	for _, v := range values {
		r *= v.SmallRealValue()
	}
	return form.Real(r), true, nil
})

var divideIntegerOperation = numericOperation(2, func(input form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("dividing %v", values)
	if large {
		a := values[0].LargeIntegerValue()
		b := values[1].LargeIntegerValue()
		if b.Cmp(big.NewInt(0)) == 0 {
			return input, false, nil
		}
		r := new(big.Rat).SetFrac(a, b)
		return form.LargeRational(r), true, nil
	}

	a := values[0].SmallIntegerValue()
	b := values[1].SmallIntegerValue()
	if b == 0 {
		return input, false, nil
	}
	return form.Rational(a, b), true, nil
})

var divideRationalOperation = numericOperation(2, func(input form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("dividing %v", values)
	if large {
		a := values[0].LargeRationalValue()
		b := values[1].LargeRationalValue()
		if b.Cmp(big.NewRat(0, 1)) == 0 {
			return input, false, nil
		}
		r := new(big.Rat).Quo(a, b)
		return form.LargeRational(r), true, nil
	}

	a := values[0].SmallRationalValue()
	b := values[1].SmallRationalValue()
	if b.IsZero() {
		return input, false, nil
	}
	r := a.Divide(b)
	return form.Rational(r.Numerator, r.Denominator), true, nil
})

var divideRealOperation = numericOperation(2, func(input form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("dividing %v", values)
	if large {
		a := values[0].LargeRealValue()
		b := values[1].LargeRealValue()
		if b.Cmp(big.NewFloat(0)) == 0 {
			return input, false, nil
		}
		r := new(big.Float).Quo(a, b)
		return form.LargeReal(r), true, nil
	}

	a := values[0].SmallRealValue()
	b := values[1].SmallRealValue()
	if b == 0 {
		return input, false, nil
	}
	return form.Real(a / b), true, nil
})

var gcdIntegerOperation = numericOperation(2, func(input form.Form, values []form.NumericForm, large bool) (form.Form, bool, error) {
	debug.Logger.Logf("calculating CGD of %v", values)
	if large {
		a := values[0].LargeIntegerValue()
		b := values[1].LargeIntegerValue()
		r := new(big.Int).GCD(nil, nil, a, b)
		return (*form.LargeIntegerForm)(r), true, nil
	}

	a := values[0].SmallIntegerValue()
	b := values[1].SmallIntegerValue()

	if a == 0 || b == 0 {
		return input, false, nil
	}
	if a == b {
		return values[0], true, nil
	}
	if a == 1 || b == 1 {
		return form.Integer(1), true, nil
	}

	if a < 0 {
		a = -a
	}
	if b < 0 {
		b = -b
	}

	// remainder GCD algorithm
	for b != 0 {
		a, b = b, a%b
	}

	return form.Integer(a), true, nil
})
