package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

//go:generate generate-state impl ../symbols.yml generated.go

// MUST NOT BE ALTERED after init
var builtinSymbolData = map[form.SymbolForm]*state.SymbolData{}

func init() {
	initGeneratedSymbols(builtinSymbolData)
	initMathSymbols(builtinSymbolData)
	initUUIDSymbols(builtinSymbolData)
	initPluginStates()
}

// Builtin is the context for symbols that are baked into the kernel
var Builtin = state.NewContext(nil, true, builtinSymbolData)

// Global is the context for global symbols
var Global = state.NewContext(Builtin, false, nil)

// stubs
var (
	ModulesState     = symbolInit{name: symbols.ModulesForm, attributes: attrProtected}.create()
	ConnectState     = symbolInit{name: symbols.ConnectForm, attributes: attrProtected}.create()
	ConnectionState  = symbolInit{name: symbols.ConnectionForm, attributes: attrProtected}.create()
	ConnectionsState = symbolInit{name: symbols.ConnectionsForm, attributes: attrProtected}.create()

	InnerValuesState = symbolInit{name: symbols.InnerValuesForm, attributes: attrProtected}.create()
	OuterValuesState = symbolInit{name: symbols.OuterValuesForm, attributes: attrProtected}.create()
	OwnValuesState   = symbolInit{name: symbols.OwnValuesForm, attributes: attrProtected}.create()

	PowerState = symbolInit{name: symbols.PowerForm, attributes: attrProtected}.create()
)
