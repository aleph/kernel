package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
)

func newNumScope2() kernel.Scope { return new(numScope2) }

type numScope2 [2]form.NumericForm

func (s *numScope2) Set(name string, value form.Form) kernel.Scope {
	switch name {
	case "x1":
		s[0], _ = value.(form.NumericForm)
	case "x2":
		s[1], _ = value.(form.NumericForm)
	}
	return s
}

func (s *numScope2) Get(name string) (form.Form, bool) {
	switch name {
	case "x1":
		return s[0], true
	case "x2":
		return s[1], true
	}
	return nil, false
}

func (s *numScope2) Enumerate(fn func(string, form.Form)) { fn("x1", s[0]); fn("x2", s[1]) }
