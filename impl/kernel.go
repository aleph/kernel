package impl

import (
	"errors"
	"sync"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/debug"
	"gitlab.com/aleph-project/kernel/recognizer"
	"gitlab.com/aleph-project/kernel/state"
)

func NewKernel() kernel.Kernel {
	return &Kernel{
		Context: state.NewContext(Global, false, nil),
	}
}

func NewGlobalKernel() kernel.Kernel {
	return &Kernel{Global}
}

type Kernel struct {
	Context *state.Context
}

func (k *Kernel) State() kernel.Context {
	return k.Context
}

func (k *Kernel) NewScope() kernel.Kernel {
	return &Kernel{
		Context: state.NewContext(k.Context, false, nil),
	}
}

func (k *Kernel) NewWithScope(scope kernel.Scope) kernel.Kernel {
	l := k.NewScope()
	l.State().Symbols().Operate(func(lock *sync.RWMutex, get kernel.SymbolTableGet, create kernel.SymbolTableCreate) error {
		lock.Lock()

		scope.Enumerate(func(name string, value form.Form) {
			sym := form.SymbolForm(name)
			create(sym).SetOwnValues(false, state.NewSymbolValue(
				sym, nil, nil,
				func(form.Form, kernel.Scope, kernel.Kernel) (form.Form, bool, error) {
					debug.Logger.Logf("local %v -> %v", sym, value)
					return value, true, nil
				},
				nil,
			))
		})

		lock.Unlock()
		return nil
	})

	return l
}

func (k *Kernel) GetKnownHeadName(id uint64) (name string, ok bool) {
	for ctx := k.State(); ctx != nil; ctx = ctx.Parent() {
		sym, ok := ctx.Symbols().GetByID(id)
		if ok {
			return sym.Name().Raw(), true
		}
	}

	return "", false
}

func (k *Kernel) GetKnownHeadID(name string) (id uint64, ok bool) {
	for ctx := k.State(); ctx != nil; ctx = ctx.Parent() {
		sym, ok := ctx.Symbols().GetByName(name)
		if ok {
			return sym.ID(), true
		}
	}

	return 0, false
}

func (k *Kernel) BuildRecognizer(predicate form.Form) (kernel.Recognizer, error) {
	return recognizer.Build(predicate)
}

func (k *Kernel) EvaluateWithScope(expr form.Form, scope kernel.Scope) (form.Form, error) {
	r, err := k.NewWithScope(scope).Evaluate(expr)
	return r, err
}

func (k *Kernel) Evaluate(expr form.Form) (form.Form, error) {
	r, _, err := k.evaluate(expr)
	return r, err
}

func (k *Kernel) evaluate(expr form.Form) (result form.Form, altered bool, err error) {
	var orig = expr
	if expr == nil {
		return form.Null, true, errors.New("expression cannot be nil")
	}
	debug.Logger.Logf("enter %v", orig)
	defer func() { debug.Logger.Logf("exit %v -> %v", orig, result) }()

	// given f(g(x))
	// own values take precedence over everything else
	// inner values of `g` take precedence over outer values of `g`
	// outer values of `g` take precedence over inner values of `f`

	// ** evaluate own value of X for Symbol(X)
	if sym, ok := expr.(form.SymbolForm); ok {
		debug.Logger.Logf("checking for own value of %v", expr)
		v, x, ok := recognizer.FindOwnValue(k, sym)
		if !ok {
			return expr, false, nil
		}

		// TODO possibly check if result should be evaluated
		debug.Logger.Logf("found own value of %v", expr)
		return v.Evaluate(expr, x, k)
	}

	// ** evaluate inner of Rational(X/Y) - allows normalization
	if _, ok := expr.(form.RationalForm); ok {
		debug.Logger.Logf("checking for inner value of %v", expr)
		v, x, ok := recognizer.FindInnerValue(k, expr)
		if !ok {
			return expr, false, nil
		}

		// TODO possibly check if result should be evaluated
		debug.Logger.Logf("found own value of %v", expr)
		return v.Evaluate(expr, x, k)
	}

	// never evaluate primitives, except symbols and rationals
	if _, ok := expr.(form.PrimitiveForm); ok {
		return expr, false, nil
	}

	head := expr.Head()
	elements := expr.GetElements()
	if expr.Depth() != uint32(len(elements)) {
		return form.Null, true, errors.New("depth does not match elements")
	}

	els := make([]form.Form, len(elements))
	copy(els, elements)

	var headSym form.SymbolForm
	formHead, isFormHead := head.(form.FormHead)

	updateHead := func(f form.Form) {
		altered = true
		if s, ok := getSymbolValue(f); ok {
			headSym = form.SymbolForm(s)
			head = form.NewHead(s, k)
		} else {
			formHead = form.NewFormHead(f)
			head = formHead
			isFormHead = true
		}
	}

	// unpack symbolic form heads
	if isFormHead {
		updateHead(formHead.Form())
	} else {
		headSym = form.SymbolForm(head.Name())
	}

	// ** evaluate own values - recurse for non-form heads
	for !isFormHead {
		debug.Logger.Logf("checking for own value of %v", expr)
		v, x, ok := recognizer.FindOwnValue(k, headSym)
		if !ok {
			break
		}

		debug.Logger.Logf("found own value of %v", expr)
		r, alt, err := v.Evaluate(expr, x, k)
		if err != nil {
			return form.Null, true, err
		}
		if !alt {
			break
		}
		updateHead(r)
	}

	// ** evaluate the head if it is a form
	if isFormHead {
		x, err := k.Evaluate(formHead.Form())
		if err != nil {
			return form.Null, true, err
		}
		updateHead(x)
	}

	var symData kernel.SymbolData
	if !isFormHead {
		symData, _ = k.Context.GetSymbol(headSym)
	}
	if symData == nil {
		symData = new(state.SymbolData)
	} else {
		debug.Logger.Logf("retrieved symbol data for %v", headSym)
	}
	var symAttrs = symData.Attributes()

	var evalEls = els
	if len(els) > 0 {
		if symAttrs.Freeze() || symAttrs.HoldAll() || symAttrs.HoldFirst() && symAttrs.HoldRest() {
			evalEls = []form.Form{}
		} else if symAttrs.HoldFirst() {
			evalEls = els[1:]
		} else if symAttrs.HoldRest() {
			evalEls = els[:1]
		}
	}

	// ** evaluate elements
	for i := range evalEls {
		el, alt, err := k.evaluate(evalEls[i])
		if err != nil {
			return form.Null, true, err
		}
		if alt {
			altered = true
			evalEls[i] = el
		}
	}

	if altered {
		debug.Logger.Logf("rebuilding expression")
		expr, err = form.New(head, els...)
		if err != nil {
			return form.Null, true, err
		}
	}

	if !symAttrs.Freeze() && len(els) > 0 {
		// ** evaluate outer values of elements
		for {
			debug.Logger.Logf("checking for outer value of %v", expr)
			v, x, ok := recognizer.FindOuterValue(k, expr)
			if !ok {
				break
			}

			debug.Logger.Logf("found outer value of %v", expr)
			var alt bool
			expr, alt, err = v.Evaluate(expr, x, k)
			if err != nil {
				return form.Null, true, err
			}
			if alt {
				altered = true
			}
		}
	}

	// ** evaluate inner value of expression
	if !isFormHead {
		debug.Logger.Logf("checking for inner value of %v", expr)
		v, x, ok := recognizer.FindInnerValue(k, expr)
		if ok {
			debug.Logger.Logf("found inner value of %v", expr)
			var alt bool
			expr, alt, err = v.Evaluate(expr, x, k)
			if err != nil {
				return form.Null, true, err
			}
			if alt {
				altered = true
			}
		}
	}

	if !altered || form.Equal(orig, expr) {
		return expr, false, nil
	}

	debug.Logger.Logf("evaluated expression to %v", expr)
	r, err := k.Evaluate(expr)
	return r, true, err
}
