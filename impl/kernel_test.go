package impl

import (
	"fmt"
	"math/rand"
	"testing"

	"github.com/alecthomas/assert"
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

var result int64

func testEval(t *testing.T, k kernel.Kernel, expr, expected form.Form) {
	result, err := k.Evaluate(expr)
	assert.NoError(t, err)
	assert.Equal(t, fmt.Sprint(result), fmt.Sprint(expected))
	assert.Equal(t, result, expected)
}

func TestRegression(t *testing.T) {
	var fHead = form.NewUnknownSymbolicHead("f")

	var tests = map[string]struct {
		expr, expect form.Form
	}{
		"Rational/Reduce": {
			expr: symbols.NewForm(
				form.NewUnknownSymbolicHead(symbols.Rational),
				form.Integer(20),
				form.Integer(30),
			),
			expect: form.Rational(2, 3),
		},
		"f[x]": {
			expr: symbols.NewForm(
				form.NewUnknownSymbolicHead("CompoundExpression"),
				symbols.NewForm(
					symbols.SetDelayedHead,
					symbols.NewForm(fHead, symbols.NewPattern(form.String("x"), symbols.NewEmptyBlank())),
					symbols.NewForm(symbols.AddHead, form.Symbol("x"), form.Integer(2)),
				),
				symbols.NewForm(fHead, form.Integer(3)),
				symbols.NewForm(fHead, form.Integer(1)),
			),
			expect: symbols.NewForm(
				form.NewUnknownSymbolicHead("CompoundExpression"),
				form.Null,
				form.Integer(5),
				form.Integer(3),
			),
		},
	}

	k := NewKernel()
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			testEval(t, k, test.expr, test.expect)
		})
	}
}

func TestSimple(t *testing.T) {
	var tests = map[string]struct {
		expr, expect form.Form
	}{
		"Add": {
			expr: symbols.NewForm(
				symbols.AddHead,
				form.Integer(1),
				form.Integer(2),
			),
			expect: form.Integer(3),
		},
		"Length/Int": {
			expr: symbols.NewForm(
				symbols.LengthHead,
				form.Integer(100),
			),
			expect: form.Integer(0),
		},
		"Length/List": {
			expr: symbols.NewForm(
				symbols.LengthHead,
				form.NewList(
					form.NewList(form.Integer(11), form.Integer(12), form.Integer(13)),
					form.NewList(form.Integer(21), form.Integer(22), form.Integer(23)),
				),
			),
			expect: form.Integer(2),
		},
		"Length/Tensor": {
			expr: symbols.NewForm(
				symbols.LengthHead,
				form.NewTensor([]uint32{3, 2},
					form.Integer(11), form.Integer(12),
					form.Integer(21), form.Integer(22),
					form.Integer(31), form.Integer(32),
				),
			),
			expect: form.Integer(6),
		},
		"Head/Tensor": {
			expr: symbols.NewForm(
				symbols.HeadHead,
				form.NewTensor([]uint32{3, 2},
					form.Integer(11), form.Integer(12),
					form.Integer(21), form.Integer(22),
					form.Integer(31), form.Integer(32),
				),
			),
			expect: form.Symbol("Tensor"),
		},
	}

	k := NewKernel()
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			testEval(t, k, test.expr, test.expect)
		})
	}
}

func TestEvalWithScope(t *testing.T) {
	k := NewKernel()

	expr, err := k.EvaluateWithScope(symbols.NewForm(
		symbols.AddHead,
		form.Integer(1),
		form.SymbolForm("x"),
	), state.NewListScope().Set("x", form.Integer(2)))
	assert.NoError(t, err)
	assert.Equal(t, expr, form.Integer(3))
}

func TestOuter(t *testing.T) {
	k := NewKernel()
	g := k.State().GetOrCreateSymbol(form.SymbolForm("g"))

	// f_(g(x_)) ^:= g(f(x))
	g.SetOuterValues(false, state.NewSymbolValue(
		symbols.NewForm(
			form.NewFormHead(symbols.NewNamedPattern("f", symbols.NewEmptyBlank())),
			symbols.NewForm(form.NewUnknownSymbolicHead("g"), symbols.NewNamedPattern("x", symbols.NewEmptyBlank())),
		), nil, nil,
		func(input form.Form, extracted kernel.Scope, _ kernel.Kernel) (form.Form, bool, error) {
			f, fok := extracted.Get("f")
			x, xok := extracted.Get("x")
			if !fok || !xok {
				t.Fatal("missing extracted parameters")
			}

			f, err := form.New(
				form.NewUnknownSymbolicHead("g"),
				symbols.NewForm(
					form.NewFormHead(f),
					x,
				),
			)
			return f, true, err
		},
		nil,
	))

	testEval(t, k,
		symbols.NewForm(
			form.NewUnknownSymbolicHead("f"),
			symbols.NewForm(
				form.NewUnknownSymbolicHead("g"),
				form.SymbolForm("x"),
			),
		),
		symbols.NewForm(
			form.NewUnknownSymbolicHead("g"),
			symbols.NewForm(
				form.NewUnknownSymbolicHead("f"),
				form.SymbolForm("x"),
			),
		))
}

func BenchmarkAdd(b *testing.B) {
	k := NewKernel()
	x := rand.Int63()
	y := rand.Int63()
	expr := symbols.NewForm(
		symbols.AddHead,
		form.Integer(x),
		form.Integer(y),
	)

	b.Run("Basic", func(b *testing.B) {
		var z int64
		for n := 0; n < b.N; n++ {
			z = x + y
		}
		result = z
	})

	b.Run("Aleph", func(b *testing.B) {
		var z form.Form
		for n := 0; n < b.N; n++ {
			z, _ = k.Evaluate(expr)
		}
		result = int64(z.(form.SmallIntegerForm))
	})
}
