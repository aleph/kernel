package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/debug"
	"gitlab.com/aleph-project/kernel/symbols"
)

func setOperation(op func(form.Form, form.Form, bool, kernel.Context) (form.Form, bool, error)) kernel.Operation {
	return func(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
		x, ok := input.GetElement(0)
		if !ok {
			return input, false, ErrOperationInputViolatesExpectations
		}

		y, ok := input.GetElement(1)
		if !ok {
			return input, false, ErrOperationInputViolatesExpectations
		}

		var name = input.Head().Name()
		var delayed = name == symbols.SetDelayed || name == symbols.OutSetDelayed
		return op(x, y, delayed, k.State())
	}
}

var set = setOperation(func(pattern, value form.Form, delayed bool, ctx kernel.Context) (form.Form, bool, error) {
	var attrs []form.SymbolForm
	var attrData kernel.Attributes

	var ret = value
	if delayed {
		ret = form.Null
	}

	sym, ok := pattern.(form.SymbolForm)
	if ok {
		data := ctx.GetOrCreateSymbol(sym)
		if data.Attributes().Protected() {
			return symbols.NewError(form.String("Symbol is protected"), sym), true, nil
		}
		debug.Logger.Logf("setting own value for %v -> %v", sym, value)
		data.SetOwnValues(false, valueInit{
			predicate:  pattern,
			definition: symbols.NewDefinition(pattern, value),
			operation: func(_ form.Form, extracted kernel.Scope, kern kernel.Kernel) (form.Form, bool, error) {
				debug.Logger.Logf("executing %v with scope %v", sym, extracted)
				f, err := kern.EvaluateWithScope(value, extracted)
				return f, true, err
			},
		}.create())
		return ret, true, nil
	}

	_, ok = pattern.Head().(form.FormHead)
	if ok {
		goto fail
	}

	sym = form.SymbolForm(pattern.Head().Name())

	if sym != symbols.AttributesForm {
		data := ctx.GetOrCreateSymbol(sym)
		if data.Attributes().Protected() {
			return symbols.NewError(form.String("Symbol is protected"), sym), true, nil
		}
		debug.Logger.Logf("setting inner value for %v -> %v", sym, value)
		data.SetInnerValues(false, valueInit{
			predicate:  pattern,
			definition: symbols.NewDefinition(pattern, value),
			operation: func(_ form.Form, extracted kernel.Scope, kern kernel.Kernel) (form.Form, bool, error) {
				debug.Logger.Logf("executing %v with scope %v", sym, extracted)
				f, err := kern.EvaluateWithScope(value, extracted)
				return f, true, err
			},
		}.create())
		return ret, true, nil
	}

	if pattern.Depth() != 1 {
		goto fail
	}

	sym, ok = getSymbol(pattern, 0)
	if !ok {
		goto fail
	}

	if !ok || value.Head().Name() != "Tensor" {
		goto fail
	}

	attrs, ok = getSymbols(value)
	if !ok {
		goto fail
	}

	attrData = ctx.GetOrCreateSymbol(sym).Attributes()
	if attrData.Locked() {
		return symbols.NewError(form.String("Symbol is locked"), sym), true, nil
	}

	for _, attr := range attrs {
		if !attrData.Supported(attr) {
			return symbols.NewError(form.String("Attempted to set unsupported attribute"), sym, attr), true, nil
		}
	}

	attrData.Reset()
	for _, attr := range attrs {
		attrData.Set(attr)
		debug.Logger.Logf("setting attribute %v for %v", attr, sym)
	}
	return ret, true, nil

fail:
	// TODO return input, false, send error
	return symbols.NewError(form.String("Cannot find symbol to attach value to"), pattern), true, nil
})

var outSet = setOperation(func(pattern, value form.Form, delayed bool, ctx kernel.Context) (form.Form, bool, error) {
	var ret = value
	if delayed {
		ret = form.Null
	}

	var symdata = make([]kernel.SymbolData, pattern.Depth())
	for _, el := range pattern.GetElements() {
		sym, ok := getHeadSymbol(el)
		if !ok {
			continue
		}

		data := ctx.GetOrCreateSymbol(sym)
		if data.Attributes().Protected() {
			return symbols.NewError(form.String("Symbol is protected"), sym), true, nil
		}

		symdata = append(symdata, data)
	}

	if len(symdata) == 0 {
		// TODO return input, false, send error
		return symbols.NewError(form.String("Cannot find symbol to attach value to"), pattern), true, nil
	}

	for _, data := range symdata {
		debug.Logger.Logf("setting outer value for %v -> %v", data.Name(), value)
		data.SetOuterValues(false, valueInit{
			predicate:  pattern,
			definition: symbols.NewDefinition(pattern, value),
			operation: func(_ form.Form, extracted kernel.Scope, kern kernel.Kernel) (form.Form, bool, error) {
				f, err := kern.EvaluateWithScope(value, extracted)
				return f, true, err
			},
		}.create())
	}

	return ret, true, nil
})
