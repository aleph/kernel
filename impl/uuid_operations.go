package impl

import (
	"log"

	"github.com/google/uuid"
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/symbols"
)

func NewRandomUUID() (form.UUIDForm, error) {
	uuid, err := uuid.NewRandom()
	if err != nil {
		return form.UUIDForm{}, err
	}

	return form.UUIDForm(uuid), nil
}

func ParseUUID(str string) (form.UUIDForm, error) {
	uuid, err := uuid.Parse(str)
	if err != nil {
		return form.UUIDForm{}, err
	}

	return form.UUIDForm(uuid), nil
}

func uuidRandomOperation(form.Form, kernel.Scope, kernel.Kernel) (form.Form, bool, error) {
	uuid, err := NewRandomUUID()
	if err != nil {
		log.Println("An error occurred while creating a random UUID", err)
		return symbols.NewStringError("An error occurred while creating a random UUID"), true, nil
	}

	return uuid, true, nil
}

func uuidNilOperation(form.Form, kernel.Scope, kernel.Kernel) (form.Form, bool, error) {
	return form.UUID(uuid.Nil), true, nil
}

func uuidParseOperation1(input form.Form, _ kernel.Scope, _ kernel.Kernel) (form.Form, bool, error) {
	return uuidParseOperation(0, input)
}

func uuidParseOperation2(input form.Form, _ kernel.Scope, _ kernel.Kernel) (form.Form, bool, error) {
	return uuidParseOperation(1, input)
}

func uuidParseOperation(index uint32, input form.Form) (form.Form, bool, error) {
	id, ok := getString(input, index)
	if !ok {
		return input, false, ErrOperationInputViolatesExpectations
	}

	uuid, err := ParseUUID(id.Raw())
	if err != nil {
		// TODO send error return ()
		return symbols.NewStringError("An error occurred while parsing the UUID", err.Error(), id.Raw()), true, nil
	}

	return uuid, true, nil
}
