package impl

import (
	"gitlab.com/aleph-project/kernel/symbols"
)

var LoadPluginState = symbolInit{
	name: symbols.LoadPluginForm,

	attributes: attrLocked,
}.create()

var PluginState = symbolInit{
	name: symbols.PluginForm,

	attributes: attrLocked,
}.create()

var PluginsState = symbolInit{
	name: symbols.PluginsForm,

	attributes: attrLocked,
}.create()
