package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

func initGeneratedSymbols(data map[form.SymbolForm]*state.SymbolData) {
	data[symbols.AddForm] = AddState
	data[symbols.AttributesForm] = AttributesState
	data[symbols.BlockForm] = BlockState
	data[symbols.ClearAttributesForm] = ClearAttributesState
	data[symbols.ConnectForm] = ConnectState
	data[symbols.ConnectionForm] = ConnectionState
	data[symbols.ConnectionsForm] = ConnectionsState
	data[symbols.DimensionsForm] = DimensionsState
	data[symbols.DivideForm] = DivideState
	data[symbols.GCDForm] = GCDState
	data[symbols.HeadForm] = HeadState
	data[symbols.InnerValuesForm] = InnerValuesState
	data[symbols.LengthForm] = LengthState
	data[symbols.LoadPluginForm] = LoadPluginState
	data[symbols.ModulesForm] = ModulesState
	data[symbols.MultiplyForm] = MultiplyState
	data[symbols.NForm] = NState
	data[symbols.OutSetForm] = OutSetState
	data[symbols.OutSetDelayedForm] = OutSetDelayedState
	data[symbols.OuterValuesForm] = OuterValuesState
	data[symbols.OwnValuesForm] = OwnValuesState
	data[symbols.PartForm] = PartState
	data[symbols.PluginForm] = PluginState
	data[symbols.PluginsForm] = PluginsState
	data[symbols.PowerForm] = PowerState
	data[symbols.SetForm] = SetState
	data[symbols.SetAttributesForm] = SetAttributesState
	data[symbols.SetDelayedForm] = SetDelayedState
	data[symbols.SubtractForm] = SubtractState
}
