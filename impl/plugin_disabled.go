// +build !plugins

package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

func initPluginStates() {
	LoadPluginState.SetInnerValues(false, valueInit{
		makeScope: state.NullScope,
		predicate: symbols.NewForm(
			symbols.LoadPluginHead,
			symbols.NewPattern(symbols.NewBlank(symbols.String)),
		),
		operation: loadPluginOperation,
	}.create())
}

func loadPluginOperation(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
	return symbols.NewStringError("Plugin loading is not enabled"), true, nil
}
