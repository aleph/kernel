package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

var BlockState = symbolInit{
	name: symbols.BlockForm,

	attributes: attrLocked,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewForm(
				symbols.BlockHead,
				// symbols.NewPattern(symbols.NewEmptyBlankNullSequence()),
				symbols.NewPattern(symbols.NewEmptyBlank()),
				symbols.NewPattern(symbols.NewEmptyBlank()),
			),
			operation: func(input form.Form, _ kernel.Scope, _ kernel.Kernel) (form.Form, bool, error) {
				var n = input.Depth()
				if n == 0 {
					return input, false, nil
				}
				el, _ := input.GetElement(n - 1)
				if el == nil {
					// TODO send error
					return input, false, nil
				}
				return el, true, nil
			},
		}.create(),
	},
}.create()

var HeadState = symbolInit{
	name: symbols.HeadForm,

	attributes: attrProtected,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewForm(
				symbols.HeadHead,
				symbols.NewPattern(symbols.NewEmptyBlank()),
			),
			operation: func(input form.Form, _ kernel.Scope, _ kernel.Kernel) (form.Form, bool, error) {
				x, ok := input.GetElement(0)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}
				return getHead(x)
			},
		}.create(),
	},
}.create()
