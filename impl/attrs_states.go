package impl

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

var x = state.NewSymbolData(symbols.AttributesForm)

var AttributesState = symbolInit{
	name: symbols.AttributesForm,

	attributes: attrProtected,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewForm(
				symbols.AttributesHead,
				symbols.NewPattern(symbols.NewBlank("Symbol")),
			),
			operation: func(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
				sym, ok := getSymbol(input, 0)
				if !ok {
					return input, false, ErrOperationInputViolatesExpectations
				}

				data, ok := k.State().GetSymbol(sym)
				if !ok {
					return form.NewList(), true, nil
				}

				symbols := data.Attributes().Symbolize()
				result := make([]form.Form, len(symbols))
				for i, s := range symbols {
					result[i] = s
				}
				return form.NewList(result...), true, nil
			},
		}.create(),
	},
}.create()

var SetAttributesState = symbolInit{
	name: symbols.SetAttributesForm,

	attributes: attrProtected,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewForm(
				symbols.SetAttributesHead,
				symbols.NewPattern(symbols.NewBlank("Symbol")),
				symbols.NewPattern(symbols.NewBlank("Symbol")),
			),
			operation: func(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
				values, ok := getSymbols(input)
				if !ok || len(values) != 2 {
					return input, false, ErrOperationInputViolatesExpectations
				}

				attrs := k.State().GetOrCreateSymbol(values[0]).Attributes()
				if attrs.Locked() {
					return symbols.NewError(form.String("Symbol is locked"), values[0]), true, nil
				}

				if attrs.Set(values[1]) {
					return form.Null, true, nil
				}

				return symbols.NewError(form.String("Attempted to set unsupported attribute"), values[0], values[1]), true, nil
			},
		}.create(),
	},
}.create()

var ClearAttributesState = symbolInit{
	name: symbols.ClearAttributesForm,

	attributes: attrProtected,
	innerValues: []kernel.SymbolValue{
		valueInit{
			makeScope: state.NullScope,
			predicate: symbols.NewForm(
				symbols.ClearAttributesHead,
				symbols.NewPattern(symbols.NewBlank("Symbol")),
				symbols.NewPattern(symbols.NewBlank("Symbol")),
			),
			operation: func(input form.Form, _ kernel.Scope, k kernel.Kernel) (form.Form, bool, error) {
				values, ok := getSymbols(input)
				if !ok || len(values) != 2 {
					return input, false, ErrOperationInputViolatesExpectations
				}

				attrs := k.State().GetOrCreateSymbol(values[0]).Attributes()
				if attrs.Locked() {
					return symbols.NewError(form.String("Symbol is locked"), values[0]), true, nil
				}

				if attrs.Clear(values[1]) {
					return form.Null, true, nil
				}

				return symbols.NewError(form.String("Attempted to clear unsupported attribute"), values[0], values[1]), true, nil
			},
		}.create(),
	},
}.create()
