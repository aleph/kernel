package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"sort"

	"github.com/go-yaml/yaml"
)

const header = `
package %s

import "gitlab.com/aleph-project/form"`

const itemFmt = `

var (
	// %[1]s %[2]s
	%[1]s = "%[1]s"

	// %[1]sForm is the symbol form of %[1]s
	%[1]sForm = form.SymbolForm(%[1]s)

	// %[1]sHead is the head for %[1]s forms
	%[1]sHead = form.NewUnknownSymbolicHead(%[1]s)
)`

func main() {
	if len(os.Args) != 4 {
		os.Exit(1)
	}

	var err error
	var src io.Reader
	var dst io.Writer

	var pkg = os.Args[1]
	var asrc = os.Args[2]
	var adst = os.Args[3]

	if asrc == "-" {
		src = os.Stdin
	} else {
		src, err = os.Open(asrc)
		if err != nil {
			log.Fatal(err)
		}
	}

	if adst == "-" {
		dst = os.Stdout
	} else {
		dst, err = os.Create(adst)
		if err != nil {
			log.Fatal(err)
		}
	}

	var data symbolFile
	err = yaml.NewDecoder(src).Decode(&data)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(dst, header[1:], pkg)
	for _, sym := range data.Sorted() {
		fmt.Fprintf(dst, itemFmt, sym.Name, sym.Description)
	}
}

type symbolFile map[string]map[string]*symbol

func (s symbolFile) Sorted() []*symbol {
	var symbols = symbols{}
	for _, items := range s {
		for item, sym := range items {
			if sym == nil {
				sym = &symbol{Name: item}
			} else if sym.Name == "" {
				sym.Name = item
			}
			symbols = append(symbols, sym)
		}
	}

	sort.Stable(symbols)
	return symbols
}

type symbol struct {
	Name, Description string
	Symbolic          bool
}

type symbols []*symbol

func (s symbols) Len() int           { return len(s) }
func (s symbols) Less(i, j int) bool { return s[i].Name < s[j].Name }
func (s symbols) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
