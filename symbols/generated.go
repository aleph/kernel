package symbols

import "gitlab.com/aleph-project/form"

var (
	// Add performs addition
	Add = "Add"

	// AddForm is the symbol form of Add
	AddForm = form.SymbolForm(Add)

	// AddHead is the head for Add forms
	AddHead = form.NewUnknownSymbolicHead(Add)
)

var (
	// Alternatives 
	Alternatives = "Alternatives"

	// AlternativesForm is the symbol form of Alternatives
	AlternativesForm = form.SymbolForm(Alternatives)

	// AlternativesHead is the head for Alternatives forms
	AlternativesHead = form.NewUnknownSymbolicHead(Alternatives)
)

var (
	// Attributes returns the attributes of a symbol
	Attributes = "Attributes"

	// AttributesForm is the symbol form of Attributes
	AttributesForm = form.SymbolForm(Attributes)

	// AttributesHead is the head for Attributes forms
	AttributesHead = form.NewUnknownSymbolicHead(Attributes)
)

var (
	// Blank matches any value, or any value with a specific head
	Blank = "Blank"

	// BlankForm is the symbol form of Blank
	BlankForm = form.SymbolForm(Blank)

	// BlankHead is the head for Blank forms
	BlankHead = form.NewUnknownSymbolicHead(Blank)
)

var (
	// BlankNullSequence matches zero or more values, or zero or more values with a specific head
	BlankNullSequence = "BlankNullSequence"

	// BlankNullSequenceForm is the symbol form of BlankNullSequence
	BlankNullSequenceForm = form.SymbolForm(BlankNullSequence)

	// BlankNullSequenceHead is the head for BlankNullSequence forms
	BlankNullSequenceHead = form.NewUnknownSymbolicHead(BlankNullSequence)
)

var (
	// BlankSequence matches one or more values, or one or more values with a specific head
	BlankSequence = "BlankSequence"

	// BlankSequenceForm is the symbol form of BlankSequence
	BlankSequenceForm = form.SymbolForm(BlankSequence)

	// BlankSequenceHead is the head for BlankSequence forms
	BlankSequenceHead = form.NewUnknownSymbolicHead(BlankSequence)
)

var (
	// Block returns its last element
	Block = "Block"

	// BlockForm is the symbol form of Block
	BlockForm = form.SymbolForm(Block)

	// BlockHead is the head for Block forms
	BlockHead = form.NewUnknownSymbolicHead(Block)
)

var (
	// ClearAttributes clears attributes of a symbol
	ClearAttributes = "ClearAttributes"

	// ClearAttributesForm is the symbol form of ClearAttributes
	ClearAttributesForm = form.SymbolForm(ClearAttributes)

	// ClearAttributesHead is the head for ClearAttributes forms
	ClearAttributesHead = form.NewUnknownSymbolicHead(ClearAttributes)
)

var (
	// Connect creates a connection to a remote Aleph kernel
	Connect = "Connect"

	// ConnectForm is the symbol form of Connect
	ConnectForm = form.SymbolForm(Connect)

	// ConnectHead is the head for Connect forms
	ConnectHead = form.NewUnknownSymbolicHead(Connect)
)

var (
	// Connection represents a connection to a remote Aleph kernel
	Connection = "Connection"

	// ConnectionForm is the symbol form of Connection
	ConnectionForm = form.SymbolForm(Connection)

	// ConnectionHead is the head for Connection forms
	ConnectionHead = form.NewUnknownSymbolicHead(Connection)
)

var (
	// Connections lists connected Aleph kernels
	Connections = "Connections"

	// ConnectionsForm is the symbol form of Connections
	ConnectionsForm = form.SymbolForm(Connections)

	// ConnectionsHead is the head for Connections forms
	ConnectionsHead = form.NewUnknownSymbolicHead(Connections)
)

var (
	// Dimensions returns the dimensions of a tensor
	Dimensions = "Dimensions"

	// DimensionsForm is the symbol form of Dimensions
	DimensionsForm = form.SymbolForm(Dimensions)

	// DimensionsHead is the head for Dimensions forms
	DimensionsHead = form.NewUnknownSymbolicHead(Dimensions)
)

var (
	// Divide performs division
	Divide = "Divide"

	// DivideForm is the symbol form of Divide
	DivideForm = form.SymbolForm(Divide)

	// DivideHead is the head for Divide forms
	DivideHead = form.NewUnknownSymbolicHead(Divide)
)

var (
	// Error represents an evaluation error
	Error = "Error"

	// ErrorForm is the symbol form of Error
	ErrorForm = form.SymbolForm(Error)

	// ErrorHead is the head for Error forms
	ErrorHead = form.NewUnknownSymbolicHead(Error)
)

var (
	// Evaluate ignores hold attributes and is transparent to the evaluator
	Evaluate = "Evaluate"

	// EvaluateForm is the symbol form of Evaluate
	EvaluateForm = form.SymbolForm(Evaluate)

	// EvaluateHead is the head for Evaluate forms
	EvaluateHead = form.NewUnknownSymbolicHead(Evaluate)
)

var (
	// Freeze completely blocks evaluation of all elements
	Freeze = "Freeze"

	// FreezeForm is the symbol form of Freeze
	FreezeForm = form.SymbolForm(Freeze)

	// FreezeHead is the head for Freeze forms
	FreezeHead = form.NewUnknownSymbolicHead(Freeze)
)

var (
	// FreezeAll is an attribute that completely blocks evaluation of all elements
	FreezeAll = "FreezeAll"

	// FreezeAllForm is the symbol form of FreezeAll
	FreezeAllForm = form.SymbolForm(FreezeAll)

	// FreezeAllHead is the head for FreezeAll forms
	FreezeAllHead = form.NewUnknownSymbolicHead(FreezeAll)
)

var (
	// GCD calculates the greatest common divisor of two numbers
	GCD = "GCD"

	// GCDForm is the symbol form of GCD
	GCDForm = form.SymbolForm(GCD)

	// GCDHead is the head for GCD forms
	GCDHead = form.NewUnknownSymbolicHead(GCD)
)

var (
	// Head returns the head of a form
	Head = "Head"

	// HeadForm is the symbol form of Head
	HeadForm = form.SymbolForm(Head)

	// HeadHead is the head for Head forms
	HeadHead = form.NewUnknownSymbolicHead(Head)
)

var (
	// Hold blocks evaluation of all elements
	Hold = "Hold"

	// HoldForm is the symbol form of Hold
	HoldForm = form.SymbolForm(Hold)

	// HoldHead is the head for Hold forms
	HoldHead = form.NewUnknownSymbolicHead(Hold)
)

var (
	// HoldAll is an attribute that blocks evaluation of all elements
	HoldAll = "HoldAll"

	// HoldAllForm is the symbol form of HoldAll
	HoldAllForm = form.SymbolForm(HoldAll)

	// HoldAllHead is the head for HoldAll forms
	HoldAllHead = form.NewUnknownSymbolicHead(HoldAll)
)

var (
	// HoldFirst is an attribute that blocks evaluation of the first element
	HoldFirst = "HoldFirst"

	// HoldFirstForm is the symbol form of HoldFirst
	HoldFirstForm = form.SymbolForm(HoldFirst)

	// HoldFirstHead is the head for HoldFirst forms
	HoldFirstHead = form.NewUnknownSymbolicHead(HoldFirst)
)

var (
	// HoldPattern blocks evaluation of all elements and is transparent to the pattern recognizer
	HoldPattern = "HoldPattern"

	// HoldPatternForm is the symbol form of HoldPattern
	HoldPatternForm = form.SymbolForm(HoldPattern)

	// HoldPatternHead is the head for HoldPattern forms
	HoldPatternHead = form.NewUnknownSymbolicHead(HoldPattern)
)

var (
	// HoldRest is an attribute that blocks evaluation of all but the first element
	HoldRest = "HoldRest"

	// HoldRestForm is the symbol form of HoldRest
	HoldRestForm = form.SymbolForm(HoldRest)

	// HoldRestHead is the head for HoldRest forms
	HoldRestHead = form.NewUnknownSymbolicHead(HoldRest)
)

var (
	// InnerValues returns the inner values of a symbol
	InnerValues = "InnerValues"

	// InnerValuesForm is the symbol form of InnerValues
	InnerValuesForm = form.SymbolForm(InnerValues)

	// InnerValuesHead is the head for InnerValues forms
	InnerValuesHead = form.NewUnknownSymbolicHead(InnerValues)
)

var (
	// Length returns the length of a form
	Length = "Length"

	// LengthForm is the symbol form of Length
	LengthForm = form.SymbolForm(Length)

	// LengthHead is the head for Length forms
	LengthHead = form.NewUnknownSymbolicHead(Length)
)

var (
	// LoadPlugin loads a plugin
	LoadPlugin = "LoadPlugin"

	// LoadPluginForm is the symbol form of LoadPlugin
	LoadPluginForm = form.SymbolForm(LoadPlugin)

	// LoadPluginHead is the head for LoadPlugin forms
	LoadPluginHead = form.NewUnknownSymbolicHead(LoadPlugin)
)

var (
	// Locked is an attribute that prevents modification of symbol attributes
	Locked = "Locked"

	// LockedForm is the symbol form of Locked
	LockedForm = form.SymbolForm(Locked)

	// LockedHead is the head for Locked forms
	LockedHead = form.NewUnknownSymbolicHead(Locked)
)

var (
	// MachinePrecision 
	MachinePrecision = "MachinePrecision"

	// MachinePrecisionForm is the symbol form of MachinePrecision
	MachinePrecisionForm = form.SymbolForm(MachinePrecision)

	// MachinePrecisionHead is the head for MachinePrecision forms
	MachinePrecisionHead = form.NewUnknownSymbolicHead(MachinePrecision)
)

var (
	// Modules lists loaded modules
	Modules = "Modules"

	// ModulesForm is the symbol form of Modules
	ModulesForm = form.SymbolForm(Modules)

	// ModulesHead is the head for Modules forms
	ModulesHead = form.NewUnknownSymbolicHead(Modules)
)

var (
	// Multiply performs multiplication
	Multiply = "Multiply"

	// MultiplyForm is the symbol form of Multiply
	MultiplyForm = form.SymbolForm(Multiply)

	// MultiplyHead is the head for Multiply forms
	MultiplyHead = form.NewUnknownSymbolicHead(Multiply)
)

var (
	// N 
	N = "N"

	// NForm is the symbol form of N
	NForm = form.SymbolForm(N)

	// NHead is the head for N forms
	NHead = form.NewUnknownSymbolicHead(N)
)

var (
	// Numeric 
	Numeric = "Numeric"

	// NumericForm is the symbol form of Numeric
	NumericForm = form.SymbolForm(Numeric)

	// NumericHead is the head for Numeric forms
	NumericHead = form.NewUnknownSymbolicHead(Numeric)
)

var (
	// OutSet assigns an outer value to one or more symbols
	OutSet = "OutSet"

	// OutSetForm is the symbol form of OutSet
	OutSetForm = form.SymbolForm(OutSet)

	// OutSetHead is the head for OutSet forms
	OutSetHead = form.NewUnknownSymbolicHead(OutSet)
)

var (
	// OutSetDelayed assigns an unevaluated outer value to one or more symbols
	OutSetDelayed = "OutSetDelayed"

	// OutSetDelayedForm is the symbol form of OutSetDelayed
	OutSetDelayedForm = form.SymbolForm(OutSetDelayed)

	// OutSetDelayedHead is the head for OutSetDelayed forms
	OutSetDelayedHead = form.NewUnknownSymbolicHead(OutSetDelayed)
)

var (
	// OuterValues returns the outer values of a symbol
	OuterValues = "OuterValues"

	// OuterValuesForm is the symbol form of OuterValues
	OuterValuesForm = form.SymbolForm(OuterValues)

	// OuterValuesHead is the head for OuterValues forms
	OuterValuesHead = form.NewUnknownSymbolicHead(OuterValues)
)

var (
	// OwnValues returns the own values of a symbol
	OwnValues = "OwnValues"

	// OwnValuesForm is the symbol form of OwnValues
	OwnValuesForm = form.SymbolForm(OwnValues)

	// OwnValuesHead is the head for OwnValues forms
	OwnValuesHead = form.NewUnknownSymbolicHead(OwnValues)
)

var (
	// Part returns the specified part of a form
	Part = "Part"

	// PartForm is the symbol form of Part
	PartForm = form.SymbolForm(Part)

	// PartHead is the head for Part forms
	PartHead = form.NewUnknownSymbolicHead(Part)
)

var (
	// Pattern represents a named pattern
	Pattern = "Pattern"

	// PatternForm is the symbol form of Pattern
	PatternForm = form.SymbolForm(Pattern)

	// PatternHead is the head for Pattern forms
	PatternHead = form.NewUnknownSymbolicHead(Pattern)
)

var (
	// Plugin represents a loaded plugin
	Plugin = "Plugin"

	// PluginForm is the symbol form of Plugin
	PluginForm = form.SymbolForm(Plugin)

	// PluginHead is the head for Plugin forms
	PluginHead = form.NewUnknownSymbolicHead(Plugin)
)

var (
	// Plugins lists loaded plugins
	Plugins = "Plugins"

	// PluginsForm is the symbol form of Plugins
	PluginsForm = form.SymbolForm(Plugins)

	// PluginsHead is the head for Plugins forms
	PluginsHead = form.NewUnknownSymbolicHead(Plugins)
)

var (
	// Power performs exponentiation
	Power = "Power"

	// PowerForm is the symbol form of Power
	PowerForm = form.SymbolForm(Power)

	// PowerHead is the head for Power forms
	PowerHead = form.NewUnknownSymbolicHead(Power)
)

var (
	// Protected is an attribute that prevents modification of symbol values
	Protected = "Protected"

	// ProtectedForm is the symbol form of Protected
	ProtectedForm = form.SymbolForm(Protected)

	// ProtectedHead is the head for Protected forms
	ProtectedHead = form.NewUnknownSymbolicHead(Protected)
)

var (
	// Rule 
	Rule = "Rule"

	// RuleForm is the symbol form of Rule
	RuleForm = form.SymbolForm(Rule)

	// RuleHead is the head for Rule forms
	RuleHead = form.NewUnknownSymbolicHead(Rule)
)

var (
	// RuleDelayed 
	RuleDelayed = "RuleDelayed"

	// RuleDelayedForm is the symbol form of RuleDelayed
	RuleDelayedForm = form.SymbolForm(RuleDelayed)

	// RuleDelayedHead is the head for RuleDelayed forms
	RuleDelayedHead = form.NewUnknownSymbolicHead(RuleDelayed)
)

var (
	// Sequence 
	Sequence = "Sequence"

	// SequenceForm is the symbol form of Sequence
	SequenceForm = form.SymbolForm(Sequence)

	// SequenceHead is the head for Sequence forms
	SequenceHead = form.NewUnknownSymbolicHead(Sequence)
)

var (
	// Set assigns a value to a symbol
	Set = "Set"

	// SetForm is the symbol form of Set
	SetForm = form.SymbolForm(Set)

	// SetHead is the head for Set forms
	SetHead = form.NewUnknownSymbolicHead(Set)
)

var (
	// SetAttributes sets attributes of a symbol
	SetAttributes = "SetAttributes"

	// SetAttributesForm is the symbol form of SetAttributes
	SetAttributesForm = form.SymbolForm(SetAttributes)

	// SetAttributesHead is the head for SetAttributes forms
	SetAttributesHead = form.NewUnknownSymbolicHead(SetAttributes)
)

var (
	// SetDelayed assigns an unevaluated value to a symbol
	SetDelayed = "SetDelayed"

	// SetDelayedForm is the symbol form of SetDelayed
	SetDelayedForm = form.SymbolForm(SetDelayed)

	// SetDelayedHead is the head for SetDelayed forms
	SetDelayedHead = form.NewUnknownSymbolicHead(SetDelayed)
)

var (
	// Subtract performs subtraction
	Subtract = "Subtract"

	// SubtractForm is the symbol form of Subtract
	SubtractForm = form.SymbolForm(Subtract)

	// SubtractHead is the head for Subtract forms
	SubtractHead = form.NewUnknownSymbolicHead(Subtract)
)

var (
	// Unevaluated blocks evaluation of all elements and is transparent to the evaluator
	Unevaluated = "Unevaluated"

	// UnevaluatedForm is the symbol form of Unevaluated
	UnevaluatedForm = form.SymbolForm(Unevaluated)

	// UnevaluatedHead is the head for Unevaluated forms
	UnevaluatedHead = form.NewUnknownSymbolicHead(Unevaluated)
)