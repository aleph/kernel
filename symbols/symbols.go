package symbols

import (
	"fmt"

	"gitlab.com/aleph-project/form"
)

//go:generate generate-symbols symbols ../symbols.yml generated.go

// types defined by package form
var (
	Integer  = "Integer"
	Real     = "Real"
	Rational = "Rational"
	String   = "String"
	Symbol   = "Symbol"
	Boolean  = "Boolean"
	UUID     = "UUID"
	Null     = "Null"

	IntegerForm  = form.SymbolForm(Integer)
	RealForm     = form.SymbolForm(Real)
	RationalForm = form.SymbolForm(Rational)
	StringForm   = form.SymbolForm(String)
	SymbolForm   = form.SymbolForm(Symbol)
	BooleanForm  = form.SymbolForm(Boolean)
	UUIDForm     = form.SymbolForm(UUID)
	NullForm     = form.SymbolForm(Null)
)

func NewForm(head form.Head, forms ...form.Form) form.Form {
	return form.Must(form.New(head, forms...))
}

func NewDefinition(pattern, result form.Form) form.Form {
	return NewForm(
		RuleDelayedHead,
		NewForm(HoldPatternHead, pattern),
		result,
	)
}

func NewError(forms ...form.Form) form.Form {
	return NewForm(ErrorHead, forms...)
}

func NewStringError(msgs ...string) form.Form {
	forms := make([]form.Form, len(msgs))
	for i, msg := range msgs {
		forms[i] = form.StringForm(msg)
	}
	return NewError(forms...)
}

func NewPattern(forms ...form.Form) form.Form {
	return NewForm(PatternHead, forms...)
}

func NewPredicateWith(head form.Head, named bool, elements ...form.Form) form.Form {
	if named {
		for i, el := range elements {
			elements[i] = NewNamedPattern(fmt.Sprintf("x%d", i), el)
		}
	}

	return NewForm(head, elements...)
}

func NewPredicateWithTypes(head form.Head, named bool, types ...string) form.Form {
	var elements = make([]form.Form, len(types))
	for i, t := range types {
		elements[i] = NewBlank(t)
	}

	return NewPredicateWith(head, named, elements...)
}

func NewNamedPattern(name string, pattern form.Form) form.Form {
	return NewPattern(form.SymbolForm(name), pattern)
}

func NewAlternatives(alts ...form.Form) form.Form {
	return NewForm(AlternativesHead, alts...)
}

func NewBlank(symbol string) form.Form {
	return NewForm(BlankHead, form.SymbolForm(symbol))
}

func NewEmptyBlank() form.Form {
	return NewForm(BlankHead)
}

func NewBlankSequence(symbol string) form.Form {
	return NewForm(BlankSequenceHead, form.SymbolForm(symbol))
}

func NewEmptyBlankSequence() form.Form {
	return NewForm(BlankSequenceHead)
}

func NewBlankNullSequence(symbol string) form.Form {
	return NewForm(BlankNullSequenceHead, form.SymbolForm(symbol))
}

func NewEmptyBlankNullSequence() form.Form {
	return NewForm(BlankNullSequenceHead)
}
