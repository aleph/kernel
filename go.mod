module gitlab.com/aleph-project/kernel

require (
	github.com/alecthomas/assert v0.0.0-20170929043011-405dbfeb8e38
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1 // indirect
	github.com/google/uuid v1.1.0
	gitlab.com/aleph-project/form v0.3.1
	gitlab.com/go-utils/debug v0.1.0
	golang.org/x/sys v0.0.0-20181228144115-9a3f9b0469bb // indirect
)

replace gitlab.com/aleph-project/form => ../form
