# Pattern Specificity

## Comparing Specificity

Comparing specificity of two patterns is meaningful if and only if A) an
expression exists that can satisfy both patterns and B) an expression exists
that can satisfy only one pattern. That is, comparing specificity:

  * Is meaningful when a partial intersection exists;
  * Is not meaningful when no intersection exists;
  * Is not meaningful when a complete intersection exists.

## Intersection

The intersection of two patterns is defined as a third pattern for which all
valid matches are also valid matches of the first two patterns. In practice, the
intersection itself is not calculated, but rather whether an intersection
exists. Two patterns can have: A) no intersection, i.e. no possible expression
can satisfy both patterns; B) complete intersection, i.e. any expression that
satisfies one pattern must satisfy the other; C) partial intersection, i.e. at
least one expression exists that satisfies both patterns, but complete
intersection is not guaranteed.

## Conditions

Two conditions with completely intersecting patterns are assumed to have a
partial intersection, unless their test expressions are exactly equal. It is
impossible[<sup>1</sup>][halt] to calculate the intersection of arbitrary test
expressions.

[halt]: https://en.wikipedia.org/wiki/Halting_problem