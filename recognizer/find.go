package recognizer

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/form/utils"
	"gitlab.com/aleph-project/kernel"
)

func FindOwnValue(k kernel.Kernel, symbol form.SymbolForm) (value kernel.SymbolValue, scope kernel.Scope, ok bool) {
	data, ok := k.State().GetSymbol(symbol)
	if !ok {
		return nil, nil, false
	}

	data.RLocker().Lock()

	for _, value := range data.OwnValues() {
		scope := value.NewScope()
		switch value.Predicate().Head().Name() {
		case "Symbol":
			v, ok := utils.GetSymbolValue(value.Predicate())
			if ok && v == symbol.Raw() {
				data.RLocker().Unlock()
				return value, scope, true
			}

		default:
			if value.Recognize(k, symbol, scope) {
				data.RLocker().Unlock()
				return value, scope, true
			}
		}
	}

	data.RLocker().Unlock()
	return nil, nil, false
}

func FindInnerValue(k kernel.Kernel, expr form.Form) (value kernel.SymbolValue, scope kernel.Scope, ok bool) {
	head, ok := utils.GetHeadAsSymbol(expr)
	if !ok {
		return nil, nil, false
	}

	data, ok := k.State().GetSymbol(head)
	if !ok {
		return nil, nil, false
	}

	data.RLocker().Lock()

	for _, value := range data.InnerValues() {
		scope := value.NewScope()
		if !value.Recognize(k, expr, scope) {
			continue
		}

		data.RLocker().Unlock()
		return value, scope, true
	}

	data.RLocker().Unlock()
	return nil, nil, false
}

func FindOuterValue(k kernel.Kernel, expr form.Form) (value kernel.SymbolValue, scope kernel.Scope, ok bool) {
	for _, el := range expr.GetElements() {
		head, ok := utils.GetHeadAsSymbol(el)
		if !ok {
			continue
		}

		data, ok := k.State().GetSymbol(head)
		if !ok {
			continue
		}

		for _, value := range data.OuterValues() {
			// TODO use value.Recognize()
			scope := value.NewScope()
			if !MatchOuterValue(k, value.Predicate(), expr, scope) {
				continue
			}

			return value, scope, true
		}
	}

	return nil, nil, false
}
