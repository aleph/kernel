package recognizer

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/form/utils"
	"gitlab.com/aleph-project/kernel"
)

func MatchOuterValue(k kernel.Kernel, pattern, expr form.Form, scope kernel.Scope) bool {
	return MatchPattern(k, utils.GetHeadAsForm(pattern), utils.GetHeadAsForm(expr), scope) &&
		MatchInnerValue(k, pattern, expr, scope)
}

// assumes that pattern's head and expr's head have already been matched
func MatchInnerValue(k kernel.Kernel, pattern, expr form.Form, scope kernel.Scope) bool {
	if pattern.Depth() == 0 {
		return expr.Depth() == 0
	}

	var hasOptional = false
	var hasSequence = false
	var minDepth uint32 = 0

	for _, el := range pattern.GetElements() {
		switch el.Head().Name() {
		case "BlankSequence":
			hasSequence = true
			minDepth++

		case "BlankNullSequence":
			hasSequence = true

		case "Optional":
			hasOptional = true

		default:
			minDepth++
		}
	}

	if !hasSequence && !hasOptional {
		if expr.Depth() != minDepth {
			return false
		}

		for i, p := range pattern.GetElements() {
			el, ok := expr.GetElement(uint32(i))
			if !ok {
				return false
			}

			if !MatchPattern(k, p, el, scope) {
				return false
			}
		}

		return true
	}

	if expr.Depth() < minDepth {
		return false
	}

	// TODO implement sequence and optional matching
	return false
}

func MatchPattern(k kernel.Kernel, pattern, expr form.Form, scope kernel.Scope) bool {
	switch pattern.Head().Name() {
	case "Condition":
		// get the pattern
		p, ok := pattern.GetElement(0)
		if !ok {
			return false
		}

		return MatchPattern(k, p, expr, scope) &&
			MatchCondition(k, pattern, expr, scope)

	case "Pattern":
		// continue

	default:
		return form.Compare(pattern, expr, func(a, b form.Form, _ form.Comparison) bool {
			return MatchPattern(k, a, b, scope)
		})
	}

	var depth = pattern.Depth()
	if depth < 1 || depth > 2 {
		return false
	}

	var name string
	p, ok := pattern.GetElement(0)
	if !ok {
		return false
	}
	if depth > 1 {
		name, ok = utils.GetSymbolValue(p)
		if !ok {
			return false
		}
		p, ok = pattern.GetElement(1)
		if !ok {
			return false
		}
	}

	switch p.Head().Name() {
	case "Condition":
		if !MatchCondition(k, p, expr, scope) {
			return false
		}
		scope.Set(name, expr)
		return true

	case "Blank", "BlankSequence", "BlankNullSequence":
		arg, aok := p.GetElement(0)
		sym, sok := utils.GetSymbolValue(arg)
		if aok {
			if !sok {
				// TODO maybe change?
				return false
			}
			if sym != expr.Head().Name() {
				return false
			}
		}
		scope.Set(name, expr)
		return true

	default:
		return form.Compare(pattern, expr, func(a, b form.Form, _ form.Comparison) bool {
			return MatchPattern(k, a, b, scope)
		})
	}
}

func MatchCondition(k kernel.Kernel, condition, expr form.Form, scope kernel.Scope) bool {
	// TODO implement
	return false
}
