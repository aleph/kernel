package recognizer

import (
	"testing"

	"github.com/alecthomas/assert"
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/stub"
	"gitlab.com/aleph-project/kernel/symbols"
)

func BenchmarkBuildAdd(b *testing.B) {
	predicate := form.Must(form.New(
		symbols.AddHead,
		symbols.NewNamedPattern("x", symbols.NewBlank("Integer")),
		symbols.NewNamedPattern("y", symbols.NewBlank("Integer")),
	))

	for n := 0; n < b.N; n++ {
		Build(predicate)
	}
}

func BenchmarkRecognizeAdd(b *testing.B) {
	predicate := form.Must(form.New(
		symbols.AddHead,
		symbols.NewNamedPattern("x", symbols.NewBlank("Integer")),
		symbols.NewNamedPattern("y", symbols.NewBlank("Integer")),
	))

	recognizer, err := Build(predicate)
	if err != nil {
		b.Fatal(err)
	}

	value := form.Must(form.New(
		symbols.AddHead,
		form.Integer(1),
		form.Integer(2),
	))

	k := new(stub.Kernel)
	s := state.NullScope()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		recognizer.Recognize(k, s, value)
	}
}

func testRecognizePattern(t *testing.T, recognizer kernel.Recognizer, expr form.Form, expected map[string]form.Form) {
	extracted := state.NewMapScope()
	assert.True(t, recognizer.Recognize(new(stub.Kernel), extracted, expr))
	assertExtracted(t, extracted, expected)
}

func TestRecognizeAdd(t *testing.T) {
	predicate := symbols.NewPattern(symbols.NewForm(
		symbols.AddHead,
		symbols.NewNamedPattern("x", symbols.NewBlank("Integer")),
		symbols.NewNamedPattern("y", symbols.NewBlank("Integer")),
	))

	recognizer, err := Build(predicate)
	if err != nil {
		t.Fatal(err)
	}

	value := form.Must(form.New(
		symbols.AddHead,
		form.Integer(1),
		form.Integer(2),
	))

	testRecognizePattern(t, recognizer, value, map[string]form.Form{
		"":  value,
		"x": form.Integer(1),
		"y": form.Integer(2),
	})
}
