package recognizer

import (
	"testing"

	"github.com/alecthomas/assert"
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/state"
	"gitlab.com/aleph-project/kernel/symbols"
)

func assertExtracted(t *testing.T, actual kernel.Scope, expected map[string]form.Form) {
	for k, v := range expected {
		w, ok := actual.Get(k)
		if !ok {
			t.Fatalf("expected extracted value '%s'", k)
		}

		if !form.Equal(v, w) {
			t.Errorf("extracted value '%s' has unexpected value", k)
			assert.Equal(t, v, w)
		}
	}
}

func testMatchPattern(t *testing.T, pattern, expr form.Form, expected map[string]form.Form) {
	extracted := state.NewMapScope()
	assert.True(t, MatchInnerValue(nil, pattern, expr, extracted))
	assertExtracted(t, extracted, expected)
}

func TestMatchPattern(t *testing.T) {
	pattern := form.Must(form.New(
		symbols.AddHead,
		symbols.NewNamedPattern("x", symbols.NewBlank("Integer")),
		symbols.NewNamedPattern("y", symbols.NewBlank("Integer")),
	))

	testMatchPattern(t, pattern, form.Must(form.New(
		symbols.AddHead,
		form.Integer(1),
		form.Integer(2),
	)), map[string]form.Form{
		"x": form.Integer(1),
		"y": form.Integer(2),
	})

	assert.False(t, MatchInnerValue(nil, pattern, form.Must(form.New(
		symbols.AddHead,
		form.Integer(1),
		form.Real(2),
	)), state.NullScope()))
}
