package recognizer

import (
	"errors"
	"fmt"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/form/utils"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/debug"
)

func Build(predicate form.Form) (kernel.Recognizer, error) {
	switch p := predicate.(type) {
	case form.PrimitiveForm:
		return &primitiveRecognizer{p}, nil
	}

	switch predicate.Head().Name() {
	case "Condition":
		return buildCondition(predicate)

	case "Pattern":
		return buildPattern(predicate)

	case "Blank":
		arg, ok := predicate.GetElement(0)
		if !ok {
			return &anyRecognizer{predicate, func(n uint32) bool { return n == 1 }}, nil
		}

		sym, ok := utils.GetSymbolValue(arg)
		if !ok {
			// TODO maybe support in the future?
			return nil, errors.New("Blank used as a pattern for a non-symbol")
		}
		return &blankRecognizer{
			predicate: predicate,
			name:      sym,
		}, nil

	case "Numeric":
		if predicate.Depth() != 0 {
			return nil, errors.New("Numeric used with arguments as a pattern")
		}

		return &numericRecognizer{predicate}, nil

	case "Alternatives":
		if predicate.Depth() == 0 {
			return &noneRecognizer{predicate, func(n uint32) bool { return n == 1 }}, nil
		}

		var err error
		var els = predicate.GetElements()
		var r = &alternativesRecognizer{predicate, make([]kernel.Recognizer, len(els))}
		for i := range els {
			r.alts[i], err = Build(els[i])
			if err != nil {
				return nil, err
			}
		}

		return r, nil

	case "Optional", "BlankSequence", "BlankNullSequence":
		// FIXME implement optional and sequences
		panic("unsupported operation")
	}

	r, err := buildForm(predicate)
	if err != nil {
		return nil, err
	}

	return r, nil
}

func buildPattern(pattern form.Form) (kernel.Recognizer, error) {
	var depth = pattern.Depth()
	if depth < 1 || depth > 2 {
		return nil, fmt.Errorf("expected 1 or 2 elements, got %d", depth)
	}

	var name string
	p, ok := pattern.GetElement(0)
	if !ok {
		return nil, errors.New("failed to get the first element of the pattern")
	}
	if depth > 1 {
		name, ok = utils.GetSymbolValue(p)
		if !ok {
			return nil, errors.New("first argument is not a symbol")
		}
		p, ok = pattern.GetElement(1)
		if !ok {
			return nil, errors.New("failed to get the second element of the pattern")
		}
	}

	r, err := Build(p)
	if err != nil {
		return nil, err
	}

	if name == "" {
		return r, nil
	}

	return &patternRecognizer{
		predicate: pattern,
		inner:     r,
		name:      name,
	}, nil
}

func buildCondition(condition form.Form) (kernel.Recognizer, error) {
	var err error
	var r = &conditionRecognizer{
		predicate: condition,
	}

	if condition.Depth() != 2 {
		return nil, fmt.Errorf("expected a depth of 2, got %d", condition.Depth())
	}

	// get the pattern
	p, ok := condition.GetElement(0)
	if !ok {
		return nil, errors.New("Failed to retrieve pattern from condition")
	}

	if p.Head().Name() != "Pattern" {
		return nil, errors.New("Cannot build condition: the first argument is not a pattern")
	}

	r.pattern, err = buildPattern(p)
	if err != nil {
		return nil, err
	}

	r.condition, ok = condition.GetElement(1)
	if !ok {
		return nil, errors.New("failed to get the second element of the condition")
	}

	return r, nil
}

func buildForm(predicate form.Form) (r *formRecognizer, err error) {
	r = &formRecognizer{
		predicate: predicate,
	}

	// build a recognizer for generic expressions
	r.head, err = buildHead(predicate.Head())
	if err != nil {
		return nil, err
	}

	if predicate.Depth() == 0 {
		r.depth = func(n uint32) bool { return n == 0 }
		return r, nil
	}

	var unbounded bool
	var minDepth uint32
	var maxDepth uint32

	var els = predicate.GetElements()
	for _, el := range els {
		switch el.Head().Name() {
		case "BlankSequence":
			unbounded = false
			minDepth++

		case "BlankNullSequence":
			unbounded = false

		case "Optional":
			maxDepth++

		default:
			minDepth++
			maxDepth++
		}
	}

	if unbounded {
		r.depth = func(n uint32) bool { return n >= minDepth }
	} else if minDepth == maxDepth {
		r.depth = func(n uint32) bool { return n == minDepth }
	} else {
		r.depth = func(n uint32) bool { return minDepth <= n && n <= maxDepth }
	}

	r.children = make([]kernel.Recognizer, len(els))
	for i, el := range els {
		r.children[i], err = Build(el)
		if err != nil {
			return nil, err
		}
	}

	return r, nil
}

func buildHead(predicate form.Head) (func(kernel.Kernel, kernel.Scope, form.Head) bool, error) {
	p, ok := predicate.(form.FormHead)
	if !ok {
		name := predicate.Name()
		return func(_ kernel.Kernel, _ kernel.Scope, expr form.Head) bool {
			debug.Logger.Logf("evaluating %v against %v", expr.Name(), predicate)
			sym, ok := utils.HeadAsSymbol(expr)
			return ok && sym.Raw() == name
		}, nil
	}

	r, err := Build(p.Form())
	if err != nil {
		return nil, err
	}

	return func(k kernel.Kernel, scope kernel.Scope, expr form.Head) bool {
		debug.Logger.Logf("evaluating %v against %v", expr, predicate)
		fh, ok := expr.(form.FormHead)
		if !ok {
			return false
		}
		return r.Recognize(k, scope, fh.Form())
	}, nil
}
