package recognizer

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/form/utils"
	"gitlab.com/aleph-project/kernel"
)

func scopeSet(scope kernel.Scope, key string, exprs ...form.Form) {
	if len(exprs) == 1 {
		scope.Set(key, exprs[0])
	} else {
		scope.Set(key, form.Sequence(exprs))
	}
}

type noneRecognizer struct {
	predicate form.Form
	accept    func(uint32) bool
}

func (r *noneRecognizer) Predicate() form.Form { return r.predicate }
func (r *noneRecognizer) Accept(n uint32) bool { return r.accept(n) }
func (r *noneRecognizer) Recognize(_ kernel.Kernel, scope kernel.Scope, exprs ...form.Form) bool {
	return false
}

type anyRecognizer struct {
	predicate form.Form
	accept    func(uint32) bool
}

func (r *anyRecognizer) Predicate() form.Form { return r.predicate }
func (r *anyRecognizer) Accept(n uint32) bool { return r.accept(n) }
func (r *anyRecognizer) Recognize(_ kernel.Kernel, scope kernel.Scope, exprs ...form.Form) bool {
	scopeSet(scope, "", exprs...)
	return true
}

type primitiveRecognizer struct {
	predicate form.PrimitiveForm
}

func (r *primitiveRecognizer) Predicate() form.Form { return r.predicate }
func (r *primitiveRecognizer) Accept(n uint32) bool { return n == 1 }
func (r *primitiveRecognizer) Recognize(_ kernel.Kernel, scope kernel.Scope, exprs ...form.Form) bool {
	// primitives are matched exactly
	if len(exprs) != 0 || form.Equal(r.predicate, exprs[0]) {
		return false
	}
	scope.Set("", exprs[0])
	return true
}

type numericRecognizer struct {
	predicate form.Form
}

func (r *numericRecognizer) Predicate() form.Form { return r.predicate }
func (r *numericRecognizer) Accept(n uint32) bool { return n == 1 }
func (r *numericRecognizer) Recognize(_ kernel.Kernel, scope kernel.Scope, exprs ...form.Form) bool {
	for _, expr := range exprs {
		_, ok := expr.(form.NumericForm)
		if !ok {
			return false
		}
	}

	scopeSet(scope, "", exprs...)
	return true
}

type blankRecognizer struct {
	predicate form.Form
	name      string
}

func (r *blankRecognizer) Predicate() form.Form { return r.predicate }
func (r *blankRecognizer) Accept(n uint32) bool { return true }

func (r *blankRecognizer) Recognize(_ kernel.Kernel, scope kernel.Scope, exprs ...form.Form) bool {
	if len(exprs) != 1 {
		return false
	}

	sym, ok := utils.HeadAsSymbol(exprs[0].Head())
	if !ok || sym.Raw() != r.name {
		return false
	}

	scope.Set("", exprs[0])
	return true
}

type alternativesRecognizer struct {
	predicate form.Form
	alts      []kernel.Recognizer
}

func (r *alternativesRecognizer) Predicate() form.Form { return r.predicate }
func (r *alternativesRecognizer) Accept(n uint32) bool { return n == 1 }
func (r *alternativesRecognizer) Recognize(k kernel.Kernel, scope kernel.Scope, exprs ...form.Form) bool {
	// TODO find a way to discard scopes for unmatched
	for _, r := range r.alts {
		if r.Recognize(k, scope, exprs...) {
			scopeSet(scope, "", exprs...)
			return true
		}
	}
	return false
}

type patternRecognizer struct {
	predicate form.Form
	inner     kernel.Recognizer
	name      string
}

func (r *patternRecognizer) Predicate() form.Form { return r.predicate }
func (r *patternRecognizer) Accept(n uint32) bool { return r.inner.Accept(n) }
func (r *patternRecognizer) Recognize(k kernel.Kernel, scope kernel.Scope, exprs ...form.Form) bool {
	if !r.inner.Recognize(k, scope, exprs...) {
		return false
	}
	scopeSet(scope, r.name, exprs...)
	return true
}

type conditionRecognizer struct {
	predicate form.Form
	pattern   kernel.Recognizer
	condition form.Form
}

func (r *conditionRecognizer) Predicate() form.Form { return r.predicate }
func (r *conditionRecognizer) Accept(n uint32) bool { return r.pattern.Accept(n) }
func (r *conditionRecognizer) Recognize(k kernel.Kernel, scope kernel.Scope, exprs ...form.Form) bool {
	if !r.pattern.Recognize(k, scope, exprs...) {
		return false
	}

	var expr form.Form
	if len(exprs) == 1 {
		expr = exprs[0]
	} else {
		expr = form.Sequence(exprs)
	}

	l := k.NewWithScope(scope)
	x, err := l.Evaluate(expr)
	if err != nil {
		return false
	}

	b, ok := x.(form.BooleanForm)
	if !ok || !b.Raw() {
		return false
	}

	scope.Set("", expr)
	return true
}

type formRecognizer struct {
	predicate form.Form
	head      func(kernel.Kernel, kernel.Scope, form.Head) bool
	depth     func(uint32) bool
	children  []kernel.Recognizer
}

func (r *formRecognizer) Predicate() form.Form { return r.predicate }
func (r *formRecognizer) Accept(n uint32) bool { return n == 1 }

func (r *formRecognizer) Recognize(k kernel.Kernel, scope kernel.Scope, exprs ...form.Form) bool {
	if len(exprs) != 1 {
		return false
	}

	if !r.depth(exprs[0].Depth()) {
		return false
	}

	if !r.head(k, scope, exprs[0].Head()) {
		return false
	}

	var els = exprs[0].GetElements()
	if !r.depth(uint32(len(els))) {
		return false
	}

	if len(els) != len(r.children) {
		// FIXME implement optional and sequences
		panic("unsupported operation")
	}

	for i, child := range r.children {
		if !child.Recognize(k, scope, els[i]) {
			return false
		}
	}

	scope.Set("", exprs[0])
	return true
}
