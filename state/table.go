package state

import (
	"sync"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
)

type SymbolTable struct {
	readonly bool

	lock sync.RWMutex
	data map[form.SymbolForm]*SymbolData
}

func (t *SymbolTable) Writable() bool { return !t.readonly }

func (t *SymbolTable) Operate(operation kernel.SymbolTableOperation) error {
	var get kernel.SymbolTableGet
	var create kernel.SymbolTableCreate

	get = func(s form.SymbolForm) (kernel.SymbolData, bool) { d, ok := t.data[s]; return d, ok }
	if !t.readonly {
		create = func(s form.SymbolForm) kernel.SymbolData { d := NewSymbolData(s); t.data[s] = d; return d }
	}

	return operation(&t.lock, get, create)
}

func (t *SymbolTable) Get(symbol form.SymbolForm) (kernel.SymbolData, bool) {
	t.lock.RLock()

	if data, ok := t.data[symbol]; ok {
		t.lock.RUnlock()
		return data, true
	}

	t.lock.RUnlock()
	return nil, false
}

func (t *SymbolTable) GetOrCreate(symbol form.SymbolForm) kernel.SymbolData {
	if data, ok := t.Get(symbol); ok {
		return data
	}

	t.lock.Lock()

	if data, ok := t.data[symbol]; ok {
		t.lock.Unlock()
		return data
	}

	data := NewSymbolData(symbol)
	t.data[symbol] = data
	t.lock.Unlock()
	return data
}

func (t *SymbolTable) Create(symbol form.SymbolForm) (kernel.SymbolData, bool) {
	if t.readonly {
		panic("readonly symbol table")
	}

	if _, ok := t.Get(symbol); ok {
		return nil, false
	}

	t.lock.Lock()

	if _, ok := t.data[symbol]; ok {
		t.lock.Unlock()
		return nil, false
	}

	data := NewSymbolData(symbol)
	t.data[symbol] = data
	t.lock.Unlock()
	return data, true
}

func (t *SymbolTable) GetByID(id uint64) (kernel.SymbolData, bool) {
	t.lock.RLock()

	for _, v := range t.data {
		if v.id == id {
			t.lock.RUnlock()
			return v, true
		}
	}

	t.lock.RUnlock()
	return nil, false
}

func (t *SymbolTable) GetByName(name string) (kernel.SymbolData, bool) {
	return t.Get(form.SymbolForm(name))
}
