package state

import (
	"sync/atomic"
	"unsafe"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
)

type stringer interface {
	String() string
}

func stringify(s kernel.Scope) string {
	var r = "<| "
	var first = true
	s.Enumerate(func(n string, v form.Form) {
		if !first {
			r += ", "
		} else {
			first = false
		}
		r += "'" + n + "' -> " + v.(stringer).String()
	})
	r += " |>"
	return r
}

func NullScope() kernel.Scope    { return nullScope }
func NewMapScope() kernel.Scope  { return mapScope{} }
func NewListScope() kernel.Scope { return new(listScope) }

type mapScope map[string]form.Form

func (s mapScope) Set(name string, value form.Form) kernel.Scope { s[name] = value; return s }
func (s mapScope) Get(name string) (form.Form, bool)             { v, ok := s[name]; return v, ok }
func (s mapScope) String() string                                { return stringify(s) }
func (s mapScope) Enumerate(fn func(string, form.Form)) {
	for n, v := range s {
		fn(n, v)
	}
}

var nullScope *_nullScope

type _nullScope struct{}

func (s *_nullScope) Set(name string, value form.Form) kernel.Scope { return s }
func (s *_nullScope) Get(name string) (form.Form, bool)             { return nil, false }
func (s *_nullScope) Enumerate(fn func(string, form.Form))          { panic("can't enumerate null scope") }
func (s *_nullScope) String() string                                { return "<| |>" }

type listScope struct {
	head, tail *listNode
}

type listNode struct {
	name  string
	value form.Form
	next  *listNode
}

func (s *listScope) String() string { return stringify(s) }

func (s *listScope) join(other *listScope) {
	for {
		ltail := s.tail

		addr := (*unsafe.Pointer)(unsafe.Pointer(&s.tail))
		old := unsafe.Pointer(ltail)
		new := unsafe.Pointer(other.tail)

		if !atomic.CompareAndSwapPointer(addr, old, new) {
			continue
		}

		if ltail == nil {
			s.head = other.head
		} else {
			ltail.next = other.head
		}

		return
	}
}

func (s *listScope) Set(name string, value form.Form) kernel.Scope {
	node := &listNode{name, value, nil}

	for {
		ltail := s.tail

		addr := (*unsafe.Pointer)(unsafe.Pointer(&s.tail))
		old := unsafe.Pointer(ltail)
		new := unsafe.Pointer(node)

		if !atomic.CompareAndSwapPointer(addr, old, new) {
			continue
		}

		if ltail == nil {
			s.head = node
		} else {
			ltail.next = node
		}

		return s
	}
}

func (s *listScope) Get(name string) (form.Form, bool) {
	// no locks necessary; there is technically a race condition, but it won't
	// cause errors or odd behavior

	if s == nil && s.head == nil {
		return nil, false
	}

	for n := s.head; n != nil; n = n.next {
		if n.name == name {
			return n.value, true
		}
	}

	return nil, false
}

func (s *listScope) Enumerate(fn func(string, form.Form)) {
	// no locks necessary; there is technically a race condition, but it won't
	// cause errors or odd behavior

	if s == nil && s.head == nil {
		return
	}

	for n := s.head; n != nil; n = n.next {
		fn(n.name, n.value)
	}
}
