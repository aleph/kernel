package state

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
	"gitlab.com/aleph-project/kernel/symbols"
)

func NewSymbolValue(predicate, definition, returns form.Form, operation kernel.Operation, makeScope func() kernel.Scope) *SymbolValue {
	return &SymbolValue{predicate, definition, returns, operation, makeScope, nil}
}

type SymbolValue struct {
	predicate  form.Form
	definition form.Form
	returns    form.Form
	operation  kernel.Operation

	makeScope  func() kernel.Scope
	recognizer kernel.Recognizer
}

func (v *SymbolValue) Predicate() form.Form  { return v.predicate }
func (v *SymbolValue) Definition() form.Form { return v.definition }

func (v *SymbolValue) Returns() form.Form {
	if v.returns == nil {
		v.returns = symbols.NewEmptyBlank()
	}
	return v.returns
}

func (v *SymbolValue) NewScope() kernel.Scope {
	if v.makeScope == nil {
		return new(listScope)
	}
	return v.makeScope()
}

func (v *SymbolValue) Recognize(k kernel.Kernel, expr form.Form, scope kernel.Scope) bool {
	var err error
	var r = v.recognizer

	if r != nil {
		goto end
	}

	r, err = k.BuildRecognizer(v.predicate)
	if err != nil {
		r = nullRecognizer{}
	}
	v.recognizer = r

end:
	return r.Recognize(k, scope, expr)
}

func (v *SymbolValue) Evaluate(input form.Form, extracted kernel.Scope, kernel kernel.Kernel) (result form.Form, altered bool, err error) {
	return v.operation(input, extracted, kernel)
}

type nullRecognizer struct{}

func (r nullRecognizer) Predicate() form.Form { return form.Null }
func (r nullRecognizer) Accept(n uint32) bool { return false }
func (r nullRecognizer) Recognize(_ kernel.Kernel, scope kernel.Scope, exprs ...form.Form) bool {
	return false
}
