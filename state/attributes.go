package state

import "gitlab.com/aleph-project/form"

type Attributes struct {
	// protected is for values, locked is for attributes
	protected, locked bool

	// hold affects most evaluation, but things like outer values and Evaluate are still processed
	// freeze is a complete version of hold-all that doesn't allow any evaluations
	holdFirst, holdRest, holdAll, freeze bool
}

func (a *Attributes) Protected() bool { return a.protected }
func (a *Attributes) Locked() bool    { return a.locked }
func (a *Attributes) HoldFirst() bool { return a.holdFirst }
func (a *Attributes) HoldRest() bool  { return a.holdRest }
func (a *Attributes) HoldAll() bool   { return a.holdAll }
func (a *Attributes) Freeze() bool    { return a.freeze }

func (a *Attributes) Supported(attr form.SymbolForm) bool {
	switch attr {
	case "Protected", "Locked", "HoldFirst", "HoldRest", "HoldAll", "Freeze":
		return true

	default:
		return false
	}
}

func (a *Attributes) Symbolize() []form.SymbolForm {
	var syms = []form.SymbolForm{}
	if a.protected {
		syms = append(syms, "Protected")
	}
	if a.locked {
		syms = append(syms, "Locked")
	}
	if a.holdFirst {
		syms = append(syms, "HoldFirst")
	}
	if a.holdRest {
		syms = append(syms, "HoldRest")
	}
	if a.holdAll {
		syms = append(syms, "HoldAll")
	}
	if a.freeze {
		syms = append(syms, "Freeze")
	}
	return syms
}

func (a *Attributes) Set(attr form.SymbolForm) bool {
	switch attr {
	case "Protected":
		a.protected = true
	case "Locked":
		a.locked = true
	case "HoldFirst":
		a.holdFirst = true
	case "HoldRest":
		a.holdRest = true
	case "HoldAll":
		a.holdAll = true
	case "Freeze":
		a.freeze = true

	default:
		return false
	}

	return true
}

func (a *Attributes) Clear(attr form.SymbolForm) bool {
	switch attr {
	case "Protected":
		a.protected = false
	case "Locked":
		a.locked = false
	case "HoldFirst":
		a.holdFirst = false
	case "HoldRest":
		a.holdRest = false
	case "HoldAll":
		a.holdAll = false
	case "Freeze":
		a.freeze = false

	default:
		return false
	}

	return true
}

func (a *Attributes) Reset() {
	*a = Attributes{}
}

func (a *Attributes) CloneAndSet(attrs ...form.SymbolForm) *Attributes {
	var b = *a
	for _, attr := range attrs {
		b.Set(attr)
	}
	return &b
}

func (a *Attributes) CloneAndClear(attrs ...form.SymbolForm) *Attributes {
	var b = *a
	for _, attr := range attrs {
		b.Clear(attr)
	}
	return &b
}
