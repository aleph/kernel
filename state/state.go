package state

import (
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
)

var _ kernel.Context = new(Context)
var _ kernel.SymbolTable = new(SymbolTable)
var _ kernel.SymbolData = new(SymbolData)
var _ kernel.SymbolValue = new(SymbolValue)
var _ kernel.Attributes = new(Attributes)

func NewContext(parent *Context, readonly bool, symbols map[form.SymbolForm]*SymbolData) *Context {
	if symbols == nil {
		symbols = map[form.SymbolForm]*SymbolData{}
	}

	return &Context{
		parent: parent,
		symbols: &SymbolTable{
			readonly: readonly,
			data:     symbols,
		},
	}
}

var nextID = uint64(form.MaxHeadKind)

type Context struct {
	parent *Context

	symbols *SymbolTable
}

func (c *Context) Symbols() kernel.SymbolTable { return c.symbols }

func (c *Context) Parent() kernel.Context {
	// this seems pointless, but as the return type is an interface, the return value must be an explicit nil
	if c.parent == nil {
		return nil
	}
	return c.parent
}

func (c *Context) GetSymbol(symbol form.SymbolForm) (kernel.SymbolData, bool) {
	if c == nil {
		return nil, false
	}

	if data, ok := c.symbols.Get(symbol); ok {
		return data, true
	}

	return c.parent.GetSymbol(symbol)
}

func (c *Context) GetOrCreateSymbol(symbol form.SymbolForm) kernel.SymbolData {
	// look for the data, using read locks
	if data, ok := c.GetSymbol(symbol); ok {
		return data
	}

	// write lock the whole tree
	for d := c; d != nil; d = d.parent {
		d.symbols.lock.Lock()
	}
	unlock := func() {
		for d := c; d != nil; d = d.parent {
			d.symbols.lock.Unlock()
		}
	}

	// look for the data
	for d := c; d != nil; d = d.parent {
		if data, ok := d.symbols.data[symbol]; ok {
			unlock()
			return data
		}
	}

	// create the data, unlock
	data := NewSymbolData(symbol)
	c.symbols.data[symbol] = data
	unlock()
	return data
}

func (c *Context) InstallSymbol(symbol form.SymbolForm, data kernel.SymbolData) bool {
	// look for the data, using read locks
	if _, ok := c.GetSymbol(symbol); ok {
		return false
	}

	// write lock the whole tree
	for d := c; d != nil; d = d.parent {
		d.symbols.lock.Lock()
	}
	unlock := func() {
		for d := c; d != nil; d = d.parent {
			d.symbols.lock.Unlock()
		}
	}

	// look for the data
	for d := c; d != nil; d = d.parent {
		if _, ok := d.symbols.data[symbol]; ok {
			unlock()
			return false
		}
	}

	// install the data, unlock
	// FIXME should accept any symbol data
	c.symbols.data[symbol] = data.(*SymbolData)
	unlock()
	return true
}
