package state

import (
	"sync"
	"sync/atomic"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/kernel"
)

func NewSymbolData(name form.SymbolForm) *SymbolData {
	return &SymbolData{
		id:          atomic.AddUint64(&nextID, 1),
		name:        name,
		ownValues:   []kernel.SymbolValue{},
		innerValues: []kernel.SymbolValue{},
		outerValues: []kernel.SymbolValue{},
	}
}

type SymbolData struct {
	lock sync.RWMutex

	id uint64

	name       form.SymbolForm
	attributes Attributes

	ownValues   []kernel.SymbolValue
	innerValues []kernel.SymbolValue
	outerValues []kernel.SymbolValue
}

func (d *SymbolData) Lock()   { d.lock.Lock() }
func (d *SymbolData) Unlock() { d.lock.Unlock() }

func (d *SymbolData) RLocker() sync.Locker { return d.lock.RLocker() }

func (d *SymbolData) ID() uint64                        { return d.id }
func (d *SymbolData) Name() form.SymbolForm             { return d.name }
func (d *SymbolData) Attributes() kernel.Attributes     { return &d.attributes }
func (d *SymbolData) OwnValues() []kernel.SymbolValue   { return d.ownValues }
func (d *SymbolData) InnerValues() []kernel.SymbolValue { return d.innerValues }
func (d *SymbolData) OuterValues() []kernel.SymbolValue { return d.outerValues }

func (d *SymbolData) SetOwnValues(clear bool, values ...kernel.SymbolValue) {
	d.setValues(&d.ownValues, clear, values)
}
func (d *SymbolData) SetInnerValues(clear bool, values ...kernel.SymbolValue) {
	d.setValues(&d.innerValues, clear, values)
}
func (d *SymbolData) SetOuterValues(clear bool, values ...kernel.SymbolValue) {
	d.setValues(&d.outerValues, clear, values)
}

func (d *SymbolData) setValues(values *[]kernel.SymbolValue, clear bool, add []kernel.SymbolValue) {
	d.Lock()
	if !clear {
		*values = append(*values, add...)
	} else {
		*values = add
	}
	d.Unlock()
}
