package debug

import "gitlab.com/go-utils/debug"

var Logger = debug.NewLogger("gitlab.com/aleph-project/")
